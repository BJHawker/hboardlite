object FormPlan: TFormPlan
  Left = 523
  Top = 405
  BorderStyle = bsDialog
  Caption = 'Schedule folders'
  ClientHeight = 249
  ClientWidth = 623
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 601
    Height = 201
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object btnAdd: TBitBtn
    Left = 8
    Top = 216
    Width = 65
    Height = 23
    Caption = 'Add'
    TabOrder = 1
    OnClick = btnAddClick
  end
  object btnEdit: TBitBtn
    Left = 80
    Top = 216
    Width = 65
    Height = 23
    Caption = 'Edit'
    TabOrder = 2
    OnClick = btnEditClick
  end
  object btnDel: TBitBtn
    Left = 152
    Top = 216
    Width = 65
    Height = 23
    Caption = 'Delete'
    TabOrder = 3
    OnClick = btnDelClick
  end
  object btnClose: TBitBtn
    Left = 224
    Top = 216
    Width = 65
    Height = 23
    Caption = 'Close'
    TabOrder = 4
    OnClick = btnCloseClick
  end
  object DataSource1: TDataSource
    Left = 208
    Top = 112
  end
end
