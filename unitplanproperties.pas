unit unitplanproperties;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls;

type
  TFormPlanProperties = class(TForm)
    RadioGroupDay: TRadioGroup;
    GroupBox1: TGroupBox;
    DateTimePicker1: TDateTimePicker;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    GroupBox2: TGroupBox;
    EditFolder: TEdit;
    SpeedButton1: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FormPlanProperties: TFormPlanProperties;

implementation

uses ShlObj, UnitBrowseDirectory;

{$R *.dfm}

procedure TFormPlanProperties.SpeedButton1Click(Sender: TObject);
var
  folder:string;
begin
  if true then
  begin
    // http://msdn.microsoft.com/en-us/library/bb773205%28v=vs.85%29.aspx
    folder:=BrowseDialog('Please select the configuration directory'
      ,BIF_STATUSTEXT
      or BIF_NEWDIALOGSTYLE
      or $200
      , ExtractFilePath(Application.Exename)
    );
    if folder<>'' then
    begin
      EditFolder.Text:=folder;
    end;
  end;

end;

procedure TFormPlanProperties.FormCreate(Sender: TObject);
begin
  RadioGroupDay.ItemIndex:=0;
  DateTimePicker1.Time:=StrToTime('00:00');
end;

procedure TFormPlanProperties.btnOKClick(Sender: TObject);
begin
  if EditFolder.Text<>'' then
  begin
    ModalResult:=mrOk;
  end
  else
  begin
    MessageDlg('Select folder scheme', mtWarning, [mbOk], 0);
  end;
end;

end.
