unit UNew;

interface

uses
  Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Forms, Dialogs, IdHTTP;

// Fonctions de gestion d'un CSV dans un TStringList
function LoadCSVinStrings(aFileName:string):string;
procedure SaveStringsToCSV(aFileName:string;aText:string);
function GetCSVcell(aLine:string;aIndex:integer):string;
function SetCSVcell(aLine:string;aIndex:integer;aValue:string):string;

implementation

function LoadCSVinStrings(aFileName:string): string;
var
  ts:TStringList;
begin
  ts:=TStringList.Create();
  ts.LoadFromFile(aFileName);
  if ts.Count>0 then
  begin
    while ts[ts.Count-1]='' do
    begin
      ts.Delete(ts.Count-1);
    end;
  end;
  result:=ts.Text;
  ts.Free();
end;

procedure SaveStringsToCSV(aFileName:string;aText: string);
var
  ts:TStringList;
begin
  ts:=TStringList.Create();
  ts.Text:=aText;
  ts.SaveToFile(aFileName);
  ts.Free();
end;

function GetCSVcell(aLine: string; aIndex: integer): string;
var
  ts:TStringList;
begin
  ts:=TStringList.Create();
  ts.Text:=StringReplace(aLine,';',#13#10,[rfReplaceAll]);
  if (aIndex > -1 ) and (aIndex <= ts.Count-1) then
    result:=ts[aIndex]
  else
    result:='';
  ts.Free();
end;

function SetCSVcell(aLine: string; aIndex: integer;
  aValue: string): string;
var
  ts:TStringList;
begin
  ts:=TStringList.Create();
  ts.Text:=StringReplace(aLine,';',#13#10,[rfReplaceAll]);
  while (ts.count-1<aIndex) do ts.add('');
  ts[aIndex]:=aValue;
  ts.Text:=StringReplace(ts.Text,#13#10,';',[rfReplaceAll]);
  result:=ts.Text;
  // Supprimer le dernier CR...
  result:=copy(result,1,length(result)-2);
  ts.Free();
end;

end.

