unit unitplan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, DBClient, ExtCtrls, DBCtrls, StdCtrls,
  Buttons;

type
  TFormPlan = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    btnAdd: TBitBtn;
    btnEdit: TBitBtn;
    btnDel: TBitBtn;
    btnClose: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
  private
    { D�clarations priv�es }
  public
  end;

var
  FormPlan: TFormPlan;

implementation

uses UNew, wndMain, ShlObj, UnitBrowseDirectory, unitplanproperties;

{$R *.dfm}

procedure TFormPlan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmMain.SavePlans();
end;

procedure TFormPlan.DBGrid1DblClick(Sender: TObject);
begin
  btnEditClick(Sender);
end;

procedure TFormPlan.btnAddClick(Sender: TObject);
begin
  FormPlanProperties:=TFormPlanProperties.Create(Self);
  FormPlanProperties.ShowModal();
  if FormPlanProperties.ModalResult=mrOk then
  begin
    frmMain.cdsPlan.Insert();
    frmMain.cdsPlanday.Value:=IntToStr(FormPlanProperties.RadioGroupDay.ItemIndex+1);
    frmMain.cdsPlanhour.Value:=TimeToStr(FormPlanProperties.DateTimePicker1.Time);
    frmMain.cdsPlanfolder.Value:=FormPlanProperties.EditFolder.Text;
    frmMain.cdsPlan.Post();
  end;
end;

procedure TFormPlan.btnEditClick(Sender: TObject);
begin
  if frmMain.cdsPlanday.Value<>'' then
  begin
    FormPlanProperties:=TFormPlanProperties.Create(Self);
    FormPlanProperties.RadioGroupDay.ItemIndex:=StrToInt(frmMain.cdsPlanday.Value)-1;
    FormPlanProperties.DateTimePicker1.Time:=StrToTime(frmMain.cdsPlanhour.Value);
    FormPlanProperties.EditFolder.Text:=frmMain.cdsPlanfolder.Value;
    FormPlanProperties.ShowModal();
    if FormPlanProperties.ModalResult=mrOk then
    begin
      frmMain.cdsPlan.Edit();
      frmMain.cdsPlanday.Value:=IntToStr(FormPlanProperties.RadioGroupDay.ItemIndex+1);
      frmMain.cdsPlanhour.Value:=TimeToStr(FormPlanProperties.DateTimePicker1.Time);
      frmMain.cdsPlanfolder.Value:=FormPlanProperties.EditFolder.Text;
      frmMain.cdsPlan.Post();
    end;
  end;
end;

procedure TFormPlan.btnDelClick(Sender: TObject);
begin
  if MessageDlg('Delete planning line ?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
  begin
    try
      frmMain.cdsPlan.Delete();
    except
    end;
  end;
end;

procedure TFormPlan.btnCloseClick(Sender: TObject);
begin
  Close();
end;

end.
