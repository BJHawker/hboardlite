unit wndProperties;

interface

uses
  Windows, Messages, SysUtils, Variants,
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,
  ExtDlgs;

type
  TFormProperties = class(TForm)
    EditTitle: TEdit;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    ColorDialog1: TColorDialog;
    GroupBoxText: TGroupBox;
    SpeedButtonClearText: TSpeedButton;
    GroupBoxCoordonnees: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    EditX: TEdit;
    EditY: TEdit;
    GroupBoxFont: TGroupBox;
    GroupBoxPen: TGroupBox;
    PanelPen: TPanel;
    LabelFontName: TLabel;
    LabelFontSize: TLabel;
    FontDialog1: TFontDialog;
    CheckBoxBold: TCheckBox;
    ComboBoxType: TComboBox;
    SpeedButtonItems: TSpeedButton;
    CheckBoxItalic: TCheckBox;
    BitBtn1: TBitBtn;
    CheckBoxMobile: TCheckBox;
    Button1: TButton;
    GroupBox1: TGroupBox;
    EditReserved: TEdit;
    procedure PanelColorClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButtonClearTextClick(Sender: TObject);
    procedure EditEnter(Sender: TObject);
    procedure LabelFontClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure SpeedButtonItemsClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    SavBackcolor:TColor;
    SavWidth:integer;
    SavHeight:integer;
    Focus:TWinControl;
    procedure User(var msg:TMessage); message WM_USER;
  end;

var
  FormProperties: TFormProperties;

implementation

uses wndMain, wndItems, uBoardLiteCommon;

{$R *.dfm}

procedure TFormProperties.PanelColorClick(Sender: TObject);
begin
  ColorDialog1.Color:=(Sender as TPanel).Color;
  if ColorDialog1.Execute then
  begin
    (Sender as TPanel).Color:=ColorDialog1.Color;
    (*
    if (Sender as TPanel)=PanelColor then
    begin
      SavBackcolor:=ColorDialog1.Color;
    end;
    *)
  end;
end;

procedure TFormProperties.FormCreate(Sender: TObject);
begin
  PostMessage(Handle,WM_USER, 0, 0);
end;

procedure TFormProperties.User(var msg: TMessage);
//var
//  zeScreen:Integer;
begin
  //Self.Left := (Self.Owner as TMovablePanel).left + ((Self.Owner as TMovablePanel).width - Self.width) div 2;
  //if Self.Left<0 then Self.Left:=0;
  ////Self.Left := (Self.Owner as TMovablePanel).left;
  ////Self.Top := (Self.Owner as TMovablePanel).Top + ((Self.Owner as TMovablePanel).height - Self.height) div 2;
  //Self.Top := (Self.Owner as TMovablePanel).Top;
  //if Self.Top<0 then Self.Top:=0;

  (*
  if (true) then
  begin
    zeScreen:=FormMain.ScreenOf(Self.Owner as TMovablePanel);
    Self.Left:=Screen.Monitors[zeScreen].Left
      +((Screen.Monitors[zeScreen].Width - Self.Width) div 2);
    Self.Top:=Screen.Monitors[zeScreen].Top
      +((Screen.Monitors[zeScreen].Height - Self.Height) div 2);
  end;
  *)
end;

procedure TFormProperties.SpeedButtonClearTextClick(Sender: TObject);
begin
  EditTitle.Text:='';
end;

procedure TFormProperties.EditEnter(Sender: TObject);
begin
  Focus:=(Sender as TWinControl);
end;

procedure TFormProperties.LabelFontClick(Sender: TObject);
begin
  FontDialog1.Font.color:=clBlack;
  FontDialog1.Font.Name:=LabelFontName.Caption;
  FontDialog1.Font.Size:=StrToInt(LabelFontSize.Caption);
  FontDialog1.Font.Style:=[];
  if CheckBoxBold.Checked then
    FontDialog1.Font.Style:=FontDialog1.Font.Style+[fsBold];
  if CheckBoxItalic.Checked then
    FontDialog1.Font.Style:=FontDialog1.Font.Style+[fsItalic];
  if FontDialog1.Execute then
  begin
    LabelFontName.Caption:=FontDialog1.Font.Name;
    LabelFontSize.Caption:=IntToStr(FontDialog1.Font.Size);
    CheckBoxBold.Checked:=(fsBold in FontDialog1.Font.Style);
    CheckBoxItalic.Checked:=(fsItalic in FontDialog1.Font.Style);
    (*
    if (Sender as TPanel)=PanelColor then
    begin
      SavBackcolor:=ColorDialog1.Color;
    end;
    *)
  end;
end;

procedure TFormProperties.btnOKClick(Sender: TObject);
begin
  if (true) then // EditTitle.Text<>'' then
  begin
    ModalResult:=mrOk;
  end;
end;

procedure TFormProperties.SpeedButtonItemsClick(Sender: TObject);
var
  s:string;
begin
  FormItems:=TFormItems.Create(Self);
  FormItems.ShowModal;
  if FormItems.ModalResult=mrOk then
  begin
    s:=FormItems.ListBox1.Items[FormItems.ListBox1.ItemIndex];
    s:=copy(s, 1, pos(#160, s)-1);
    EditTitle.Text:=s;
  end;
  FormItems.Release();
end;

procedure TFormProperties.BitBtn1Click(Sender: TObject);
begin
  if MessageDlg('Confirm ?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
  begin
    ComboBoxType.ItemIndex:=0;
    EditTitle.text:='';
    ModalResult:=mrOk;
  end;
end;

procedure TFormProperties.Button1Click(Sender: TObject);
begin
  try
    EditX.Text:=IntToStr(frmMain.ForceGridPosition(StrToInt(EditX.Text)));
  except
  end;
  try
    EditY.Text:=IntToStr(frmMain.ForceGridPosition(StrToInt(EditY.Text)));
  except
  end;
end;

end.
