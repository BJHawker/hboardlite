object FormEditPos: TFormEditPos
  Left = 515
  Top = 274
  BorderStyle = bsDialog
  Caption = 'Edit Products'
  ClientHeight = 414
  ClientWidth = 769
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 745
    Height = 361
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 8
    Top = 376
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 1
  end
  object DataSource1: TDataSource
    DataSet = cdsPos
    Left = 208
    Top = 112
  end
  object cdsPos: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 112
    object cdsPosCode: TStringField
      DisplayWidth = 9
      FieldName = 'Code'
    end
    object cdsPosShort: TStringField
      DisplayWidth = 28
      FieldName = 'Short'
    end
    object cdsPosLong: TStringField
      DisplayWidth = 40
      FieldName = 'Long'
    end
    object cdsPosPrice1: TStringField
      DisplayWidth = 9
      FieldName = 'Price1'
    end
    object cdsPosPrice2: TStringField
      DisplayWidth = 9
      FieldName = 'Price2'
    end
    object cdsPosPrice3: TStringField
      DisplayWidth = 9
      FieldName = 'Price3'
    end
    object cdsPosPrice4: TStringField
      DisplayWidth = 9
      FieldName = 'Price4'
    end
    object cdsPosPrice5: TStringField
      DisplayWidth = 9
      FieldName = 'Price5'
    end
    object cdsPosStock: TStringField
      FieldName = 'Stock'
    end
  end
end
