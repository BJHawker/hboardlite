object FormPlanProperties: TFormPlanProperties
  Left = 594
  Top = 516
  BorderStyle = bsDialog
  Caption = 'Planning properties'
  ClientHeight = 152
  ClientWidth = 578
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object RadioGroupDay: TRadioGroup
    Left = 8
    Top = 8
    Width = 561
    Height = 41
    Caption = 'Day'
    Columns = 7
    Items.Strings = (
      'Monday'
      'Tuesday'
      'Wednesday'
      'Thursday'
      'Friday'
      'Saturday'
      'Sunday')
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 56
    Width = 97
    Height = 57
    Caption = 'Time'
    TabOrder = 1
    object DateTimePicker1: TDateTimePicker
      Left = 8
      Top = 24
      Width = 81
      Height = 21
      Date = 41927.454585798610000000
      Time = 41927.454585798610000000
      Kind = dtkTime
      TabOrder = 0
    end
  end
  object btnOK: TBitBtn
    Left = 432
    Top = 120
    Width = 65
    Height = 23
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = btnOKClick
    NumGlyphs = 2
  end
  object btnCancel: TBitBtn
    Left = 504
    Top = 120
    Width = 65
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    NumGlyphs = 2
  end
  object GroupBox2: TGroupBox
    Left = 112
    Top = 56
    Width = 457
    Height = 57
    Caption = 'Scheme'
    TabOrder = 4
    object SpeedButton1: TSpeedButton
      Left = 416
      Top = 16
      Width = 33
      Height = 33
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object EditFolder: TEdit
      Left = 8
      Top = 24
      Width = 401
      Height = 21
      TabOrder = 0
    end
  end
end
