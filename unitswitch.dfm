object FormSwitch: TFormSwitch
  Left = 575
  Top = 274
  BorderStyle = bsDialog
  Caption = 'Switch objets screen'
  ClientHeight = 239
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TBitBtn
    Left = 160
    Top = 208
    Width = 65
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = btnOkClick
  end
  object btnCancel: TBitBtn
    Left = 232
    Top = 208
    Width = 65
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    NumGlyphs = 2
  end
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 8
    Width = 217
    Height = 193
    Caption = 'Source'
    ItemIndex = 0
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8')
    TabOrder = 2
    OnClick = RadioGroupClick
  end
  object RadioGroup2: TRadioGroup
    Left = 232
    Top = 8
    Width = 217
    Height = 193
    Caption = 'Destination'
    ItemIndex = 1
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8')
    TabOrder = 3
    OnClick = RadioGroupClick
  end
end
