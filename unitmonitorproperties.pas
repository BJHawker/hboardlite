unit unitmonitorproperties;

interface

uses
  Windows, Messages, SysUtils, Variants,
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ExtDlgs, Buttons;

type
  TFormMonitorProperties = class(TForm)
    Image1: TImage;
    LabelMonitor: TLabel;
    Label1: TLabel;
    LabelResolution: TLabel;
    Label2: TLabel;
    EditFiles: TEdit;
    RadioGroupStrech: TRadioGroup;
    BitBtn1: TBitBtn;
    OpenPictureDialog1: TOpenPictureDialog;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FormMonitorProperties: TFormMonitorProperties;

implementation

{$R *.dfm}

procedure TFormMonitorProperties.BitBtn1Click(Sender: TObject);
begin
  OpenPictureDialog1.FileName:=EditFiles.Text;
  if OpenPictureDialog1.Execute then
  begin
    EditFiles.Text:=OpenPictureDialog1.FileName;
  end;
end;

end.
