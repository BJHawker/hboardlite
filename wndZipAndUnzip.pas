unit wndZipAndUnzip;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AbMeter, AbZipper, AbBase, AbBrowse,
  AbZBrows, AbUnzper, AbArcTyp, AbUtils;

type

  TfrmZipAndUnzip = class(TForm)
    lblMain: TLabel;
    tmrBlink: TTimer;
    lblMessage: TLabel;
    UnZipper: TAbUnZipper;
    Zipper: TAbZipper;
    AbMeter1: TAbMeter;
    AbMeter2: TAbMeter;
    procedure tmrBlinkTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure UnZipperProcessItemFailure(Sender: TObject;
      Item: TAbArchiveItem; ProcessType: TAbProcessType;
      ErrorClass: TAbErrorClass; ErrorCode: Integer);
    procedure ZipperProcessItemFailure(Sender: TObject;
      Item: TAbArchiveItem; ProcessType: TAbProcessType;
      ErrorClass: TAbErrorClass; ErrorCode: Integer);
  private
    fLastError: Integer;
    { D�clarations priv�es }
  public
    function CreateArchive(_SourcePath, _DestPath, _ArchiveName : String; _Message : String; _Show : Boolean = True; _DeleteLogFileIfOk : Boolean = False; _AskOnError : Boolean = True) : Boolean; overload;
    function CreateArchive(_ItemsToSave : TStringList; _SourcePath, _DestPath, _ArchiveName : String; _Message : String; _Show : Boolean = True; _DeleteLogFileIfOk : Boolean = False; _AskOnError : Boolean = True) : Boolean; overload;
    function LoadArchive(_ArchiveName, _DestDir : String; _Message : String; _Show : Boolean = True; _AskOnError : Boolean = True) : Boolean;
    property LastError : Integer read fLastError write fLastError;
  end;

var
  frmZipAndUnzip: TfrmZipAndUnzip;

implementation

{$R *.dfm}

uses uCommon;

procedure TfrmZipAndUnzip.tmrBlinkTimer(Sender: TObject);
begin
  lblMain.Visible := not lblMain.Visible;
  Application.ProcessMessages();
end;

procedure TfrmZipAndUnzip.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := False;
end;

function TfrmZipAndUnzip.CreateArchive(_SourcePath, _DestPath, _ArchiveName : String; _Message : String; _Show : Boolean = True; _DeleteLogFileIfOk : Boolean = False; _AskOnError : Boolean = True) : Boolean;
var
  lOk : Boolean;
  sLogFileName,
  sFileName : String;
begin
  lOk := True;
  lblMain.Caption := _Message;
  If _Show then Show();
  Application.ProcessMessages();
  sLogFileName := AddSlash(ExtractFilePath(Application.ExeName)) + ExtractOnlyFileName(_ArchiveName) + '.log';
  sFileName := _ArchiveName;
  try
    LastError := 0;
    if FileExists(sLogFileName) then DeleteFile(sLogFileName);
    if FileExists(sFileName) then DeleteFile(sFileName);
    Zipper.BaseDirectory := _SourcePath;
    Zipper.LogFile := sLogFileName;
    Zipper.Filename := sFileName;
    try
      Zipper.AddFiles('*.*', 0);
    Except
      on E : Exception do
      begin
        AppendToFile(sLogFileName, 'Error on Creating Archive : ' + E.ClassName+' error raised, with message : '+E.Message);
        lOk := False;
      end;
    end;
  except
    if not _AskOnError then
      lOk := False
    else
      if MessageDlg('L''archive de sauvegarde n''a pu �tre finalis�e, Voulez vous annuler ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then Application.Terminate;
  end;
  Close();
  Zipper.Free();
  If (lOk and _DeleteLogFileIfOk) then
    DeleteFile(sLogFileName)
  else
    MoveFile(PAnsiChar(sLogFileName), PAnsiChar(_ArchiveName + '.log'));
  Result := lOk and (LastError = 0);
end;

function TfrmZipAndUnzip.CreateArchive(_ItemsToSave : TStringList; _SourcePath, _DestPath, _ArchiveName : String; _Message : String; _Show : Boolean = True; _DeleteLogFileIfOk : Boolean = False; _AskOnError : Boolean = True) : Boolean;
var
  lOk : Boolean;
  nCountfiles : Integer;
  sFileMaskToAdd,
  sLogFileName,
  sArchiveFileName : String;
begin
  lOk := True;
  lblMain.Caption := _Message;
  if _Show then Show();
  Application.ProcessMessages();
  sLogFileName := AddSlash(ExtractFilePath(Application.ExeName)) + ExtractOnlyFileName(_ArchiveName) + '.log';
  sArchiveFileName := _ArchiveName;
  try
    if FileExists(sLogFileName) then DeleteFile(sLogFileName);
    if FileExists(sArchiveFileName) then DeleteFile(sArchiveFileName);
    Zipper.BaseDirectory := _SourcePath;
    Zipper.LogFile := sLogFileName;
    Zipper.Filename := sArchiveFileName;
    Zipper.AutoSave := False;
    LastError := 0;
    for nCountfiles := 0 to _ItemsToSave.Count-1 do
      try
        if IsDirectory(AddSlash(_SourcePath) + _ItemsToSave[nCountfiles]) then
        begin
          Zipper.StoreOptions := Zipper.StoreOptions + [soRecurse];
          sFileMaskToAdd := AddSlash(_ItemsToSave[nCountfiles])+'*.*';
        end
        else
        begin
          Zipper.StoreOptions := Zipper.StoreOptions - [soRecurse];
          sFileMaskToAdd := _ItemsToSave[nCountfiles];
        end;
        Zipper.AddFiles(sFileMaskToAdd, 0);
      Except
        on E : Exception do
        begin
          AppendToFile(sLogFileName, 'Error on Creating Archive (File : ' + _ItemsToSave[nCountfiles] + ') : ' + E.ClassName+' error raised, with message : '+E.Message);
          lOk := False;
        end;
      end;
    Zipper.Save;
  except
    if not _AskOnError then
      lOk := False
    else
      if MessageDlg('L''archive de sauvegarde n''a pu �tre finalis�e, Voulez vous annuler ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then Application.Terminate;
  end;
  Close();
  Zipper.Free();
  If (lOk and _DeleteLogFileIfOk) then
    DeleteFile(sLogFileName)
  else
    MoveFile(PAnsiChar(sLogFileName), PAnsiChar(_ArchiveName + '.log'));
  Result := lOk and (LastError = 0);
end;

function TfrmZipAndUnzip.LoadArchive(_ArchiveName, _DestDir : String; _Message : String; _Show : Boolean = True; _AskOnError : Boolean = True) : Boolean;
begin
  Result := false;
  lblMain.Caption := _Message;
  If _Show then Show();
  Application.ProcessMessages();
  try
    fLastError := 0;
    Unzipper.FileName := _ArchiveName;
    Unzipper.BaseDirectory := _DestDir;
    Unzipper.ExtractFiles( '*.*' );
    Result := (fLastError = 0);
  except
    if _AskOnError then if MessageDlg('L''archive de sauvegarde n''a pu �tre finalis�e, Voulez vous annuler ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then Application.Terminate;
  end;
  Close();
end;

procedure TfrmZipAndUnzip.UnZipperProcessItemFailure(Sender: TObject;
  Item: TAbArchiveItem; ProcessType: TAbProcessType;
  ErrorClass: TAbErrorClass; ErrorCode: Integer);
begin
  LastError := ErrorCode;
end;

procedure TfrmZipAndUnzip.ZipperProcessItemFailure(Sender: TObject;
  Item: TAbArchiveItem; ProcessType: TAbProcessType;
  ErrorClass: TAbErrorClass; ErrorCode: Integer);
begin
  LastError := ErrorCode;
end;

end.
