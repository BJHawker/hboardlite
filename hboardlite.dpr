program hboardlite;

{%File 'hboardlite.ini'}
{%File 'plan.csv'}
{%File 'item.csv'}
{%File 'monitor.csv'}
{%File 'pos.csv'}

uses
  Forms,
  Windows,
  SysUtils,
  MemCheck,
  wndMain in 'wndMain.pas' {frmMain},
  wndDisplay in 'wndDisplay.pas' {frmDisplay},
  MultInst in 'multinst.pas',
  uCSV2DBfunctions in 'uCSV2DBfunctions.pas',
  UNew in 'UNew.pas',
  wndProperties in 'wndProperties.pas' {FormProperties},
  unitplan in 'unitplan.pas' {FormPlan},
  unitswitch in 'unitswitch.pas' {FormSwitch},
  wndItems in 'wndItems.pas' {FormItems},
  unitblack in 'unitblack.pas' {FormBlack},
  uniteditmonitor in 'uniteditmonitor.pas' {FormEditMonitor},
  uniteditpos in 'uniteditpos.pas' {FormEditPos},
  GraphicEx in 'GraphicEx\GraphicEx.pas',
  unitmonitorproperties in 'unitmonitorproperties.pas' {FormMonitorProperties},
  UnitBrowseDirectory in 'UnitBrowseDirectory.pas',
  unitplanproperties in 'unitplanproperties.pas' {FormPlanProperties},
  wndZipAndUnzip in 'wndZipAndUnzip.pas' {frmZipAndUnzip},
  wndLogWindow in 'wndLogWindow.pas' {frmLogWindow},
  uBoardLiteCommon in 'uBoardLiteCommon.pas',
  ExceptLog in '..\..\..\..\..\..\Program Files (x86)\Borland\Delphi7\Lib\3rdParty\ExceptLog.pas',
  uFileInUse in 'uFileInUse.pas';

{$R *.res}

begin
//  MemChk;
  Application.Initialize;
  SetWindowLong(Application.Handle, GWL_EXSTYLE, WS_EX_TOOLWINDOW);
  Application.HelpFile := 'C:\Users\Fred\Documents\Travail\hBoardLite v1.2\src\full_screen.bmp';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
