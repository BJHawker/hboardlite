unit wndLogWindow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmLogWindow = class(TForm)
    lbLog: TListBox;
    tmrHour: TTimer;
    pnlMain: TPanel;
    cmbMonitor: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tmrHourTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FList : TStringList;
  public
    procedure Display(_Log: String);
  end;

var
  frmLogWindow: TfrmLogWindow;

implementation

uses wndMain, uCommon;

{$R *.dfm}

procedure TfrmLogWindow.FormCreate(Sender: TObject);
var
  nCount : Integer;
begin
  Left := 0;
  Top := Monitor.Height - Height;

  FList := TStringList.Create;
  FList.Assign(lbLog.Items);

  Width := Monitor.Width;
  cmbMonitor.Items.Add('*All*');
  for nCount := 0 to Screen.MonitorCount-1 do
    cmbMonitor.Items.Add('Screen : ' + IntToStr(nCount+1));
  cmbMonitor.ItemIndex := 0;
end;

procedure TfrmLogWindow.Display(_Log : String);
var
  I: Integer;
begin
  FList.Add(_Log);

  lbLog.Items.BeginUpdate;
  try
    if cmbMonitor.ItemIndex > 0 then
    begin
      lbLog.Clear;
      for I := 0 to FList.Count - 1 do
        if (Pos(ActualValue(cmbMonitor), FList[I])>0) or (Pos('Screen : ', FList[I])=0) then
          lbLog.Items.Add(FList[I]);
    end
    else
      lbLog.Items.Assign(FList);
  finally
    lbLog.Items.EndUpdate;
  end;

  lbLog.ItemIndex := lbLog.Items.Count-1;

end;

procedure TfrmLogWindow.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_F4:
      if (ssAlt in Shift) then frmMain.Close()
  end;
end;

procedure TfrmLogWindow.tmrHourTimer(Sender: TObject);
begin
  Caption := FormatDateTime('YYYYMMDD-hhnnss', Now())
end;

procedure TfrmLogWindow.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FList.Free;
end;

end.
