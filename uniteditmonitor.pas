unit uniteditmonitor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, DBClient, ExtCtrls, DBCtrls, StdCtrls,
  Buttons;

type
  TFormEditMonitor = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    cdsMonitor: TClientDataSet;
    cdsMonitormonitor: TStringField;
    cdsMonitormedia: TStringField;
    btnMonitor1: TBitBtn;
    btnMonitor2: TBitBtn;
    btnMonitor3: TBitBtn;
    btnMonitor4: TBitBtn;
    btnMonitor5: TBitBtn;
    btnMonitor6: TBitBtn;
    btnMonitor7: TBitBtn;
    btnMonitor8: TBitBtn;
    btnCancel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnMonitor2Click(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    MonitorCSV:TStringList;
    procedure LoadMonitors();
    procedure SaveMonitors();
  end;

var
  FormEditMonitor: TFormEditMonitor;

implementation

uses UNew, wndMain, unitmonitorproperties;

{$R *.dfm}

procedure TFormEditMonitor.FormCreate(Sender: TObject);
var
  i:integer;
begin
  MonitorCSV:=TStringList.Create();
  LoadMonitors();
  for i:=Screen.MonitorCount-1 downto 0 do
  begin
    case i of
      0: btnMonitor1.Enabled:=true;
      1: btnMonitor2.Enabled:=true;
      2: btnMonitor3.Enabled:=true;
      3: btnMonitor4.Enabled:=true;
      4: btnMonitor5.Enabled:=true;
      5: btnMonitor6.Enabled:=true;
      6: btnMonitor7.Enabled:=true;
      7: btnMonitor8.Enabled:=true;
    end;
  end;
end;

procedure TFormEditMonitor.LoadMonitors;
begin
  MonitorCSV.LoadFromFile(ExtractFilePath(Application.Exename)+'monitor.csv');
  (*
  cdsMonitor.Close();
  cdsMonitor.CreateDataSet();
  for i:=1 to MonitorCSV.Count-1 do
  begin
    if GetCSVcell(MonitorCSV[i],0)<>'' then
    begin
      cdsMonitor.Append();
      cdsMonitormonitor.Value:=(GetCSVcell(MonitorCSV[i],0));
      cdsMonitormedia.Value:=(GetCSVcell(MonitorCSV[i],1));
      cdsMonitor.Post();
    end;
  end;
  cdsMonitor.First();
  *)
end;

procedure TFormEditMonitor.SaveMonitors;
begin
  (*
  MonitorCSV.Clear;
  MonitorCSV.add('monitor;media');
  try cdsMonitor.Post(); except end;
  cdsMonitor.First();
  while not cdsMonitor.Eof do
  begin
    s:=';'; // Attention pas trop
    s:=SetCSVcell(s,0,(cdsMonitormonitor.value));
    s:=SetCSVcell(s,1,(cdsMonitormedia.value));
    s:=copy(s,1,length(s)-1);
    MonitorCSV.add(s);
    cdsMonitor.Next();
  end;
  *)
  SaveStringsToCSV(ExtractFilePath(Application.Exename)+'monitor.csv', MonitorCSV.Text);
end;

procedure TFormEditMonitor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  // SaveMonitors();
  MonitorCSV.Free();
end;

procedure TFormEditMonitor.btnMonitor2Click(Sender: TObject);
var
  iMonitor:integer;
  StrechMode:string;
begin
  iMonitor:=(Sender as TBitBtn).Tag;
  FormMonitorProperties:=TFormMonitorProperties.Create(Self);
  FormMonitorProperties.LabelMonitor.Caption:='Monitor '+IntToStr(iMonitor);
  FormMonitorProperties.LabelResolution.Caption:=
    IntToStr(Screen.Monitors[iMonitor-1].Width)
    +' / '+IntToStr(Screen.Monitors[iMonitor-1].Height);
  FormMonitorProperties.EditFiles.Text:=(GetCSVcell(MonitorCSV[iMonitor],1));
  StrechMode:=(GetCSVcell(MonitorCSV[iMonitor],2));
  if StrechMode='+W' then FormMonitorProperties.RadioGroupStrech.ItemIndex:=1
  else if StrechMode='+H' then FormMonitorProperties.RadioGroupStrech.ItemIndex:=2
  else FormMonitorProperties.RadioGroupStrech.ItemIndex:=0;

  FormMonitorProperties.ShowModal();
  if FormMonitorProperties.ModalResult=mrOk then
  begin
    MonitorCSV[iMonitor]:=SetCSVcell(MonitorCSV[iMonitor],1,FormMonitorProperties.EditFiles.Text);
    case FormMonitorProperties.RadioGroupStrech.ItemIndex of
      0: StrechMode:='';
      1: StrechMode:='+W';
      2: StrechMode:='+H';
    end;
    MonitorCSV[iMonitor]:=SetCSVcell(MonitorCSV[iMonitor],2,StrechMode);
    SaveMonitors;
    //frmMain.DisplayBoards(); // Peut pas faire �a
  end;

  FormMonitorProperties.Release();
end;

end.
