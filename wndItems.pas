unit wndItems;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TFormItems = class(TForm)
    ListBox1: TListBox;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FormItems: TFormItems;

implementation

uses UNew, wndProperties;

{$R *.dfm}

procedure TFormItems.FormCreate(Sender: TObject);
var
  Products:TStringList;
  i:integer;
  s:string;
begin
  Products:=TStringList.Create();
  Products.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'pos.csv');
  i:=1;
  while i<Products.Count do
  begin
    s:=GetCSVcell(Products[i],0)+#160+GetCSVcell(Products[i],2);
    ListBox1.Items.Add(s);
    if FormProperties.EditTitle.Text=GetCSVcell(Products[i],0) then
    begin
      ListBox1.itemIndex:=ListBox1.Count-1;
    end;
    inc(i);
  end;
  Products.Free();
end;

procedure TFormItems.ListBox1DblClick(Sender: TObject);
begin
  btnOkClick(Sender);
end;

procedure TFormItems.ListBox1Click(Sender: TObject);
begin
  btnOk.Enabled:=(ListBox1.itemIndex>-1);
end;

procedure TFormItems.btnOkClick(Sender: TObject);
begin
  if (listBox1.itemIndex>-1) then
  begin
    ModalResult:=mrOk;
  end;
end;

end.
