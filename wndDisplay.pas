unit wndDisplay;

interface

uses
  Windows, Messages, SysUtils, Variants,
  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus, ExtCtrls,
  MPlayer, RealTimeMarquee, MultiMon, AppEvnts;

type

  TfrmDisplay = class(TForm)
    PopupMenuForm: TPopupMenu;
    Newitem1: TMenuItem;
    N1: TMenuItem;
    Quit1: TMenuItem;
    Schedulefolders1: TMenuItem;
    mnuDisplayScreen: TMenuItem;
    mnuSwitchScreens: TMenuItem;
    LabelEditMode: TLabel;
    mnuRefreshScreen: TMenuItem;
    mnuEditMonitors: TMenuItem;
    N2: TMenuItem;
    mnuEditProducts: TMenuItem;
    mnuGrid: TMenuItem;
    TimerMediaRotator: TTimer;
    X1: TMenuItem;
    N3: TMenuItem;
    mnuSaveScheme: TMenuItem;
    mnuLoadScheme: TMenuItem;
    imgMain1: TImage;
    imgMain2: TImage;
    pnlVideo: TPanel;
    mpVideo: TMediaPlayer;
    lblMonitor: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Newitem1Click(Sender: TObject);
    procedure Quit1Click(Sender: TObject);
    procedure Schedulefolders1Click(Sender: TObject);
    procedure mnuDisplayScreenClick(Sender: TObject);
    procedure mnuSwitchScreensClick(Sender: TObject);
    procedure mnuRefreshScreenClick(Sender: TObject);
    procedure mnuEditMonitorsClick(Sender: TObject);
    procedure mnuEditProductsClick(Sender: TObject);
    procedure mnuGridClick(Sender: TObject);
    procedure PopupMenuFormPopup(Sender: TObject);
    procedure TimerMediaRotatorTimer(Sender: TObject);
    procedure X1Click(Sender: TObject);
    procedure mnuSaveSchemeClick(Sender: TObject);
    procedure mnuLoadSchemeClick(Sender: TObject);
    procedure EndVideo(Sender: TObject);

    procedure DynamicLabelClick(Sender: TObject);
    procedure DynamicLabelMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DynamicLabelMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure DynamicLabelMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure mpVideoNotify(Sender: TObject);

  private

    x0,y0:integer;

    fLastThread : TThread;

    fMedias : TStringList;
    fMediaIndex : Integer;
    fStretchMode : String;

    fScreenId: Integer;

    fProcessToTerminate : THandle;

    fPlay,
    fLockByPlayer: Boolean;
    fSkipOneTurn: Boolean;
    FMediasPath: String;

    { D�clarations priv�es }
    procedure WMDeviceChange(var Msg: TMessage); message WM_DEVICECHANGE;
    procedure ResizeUponMonitor;
    procedure LoadMedia();
    procedure SetSCreenId(const Value: Integer);
    procedure SetMediaIndex(const Value: Integer);
    procedure DisplayEmptyScreen;
    function GetCurrentMedia: String;
    procedure StopMedias;
    function GetCurrentMonitor: TMonitor;
    procedure hDspBoardLog(_String : String; _Exception : Boolean = False); overload;
    procedure hDspBoardLog(_Exception : Exception; _ComplementInfo : String = ''); overload;
    procedure SetMainImage(_Image1 : Boolean);
    procedure DoCloseMain;
    function GetisVideoEnabled: Boolean;
    function IsFileAdded(_FileName: String): Boolean;
    function IsFileDeleted(_FileName: String): Boolean;

  public
    procedure PlayNextMedia();
    procedure PlayVideo(_MediaFileName : String);
    procedure StopVideo;
    procedure DisplayMedia(_MediaFileName : String = '');
    procedure GoDisplay(_Force : Boolean = False);
    procedure DisplayItems;
    procedure CheckVisibilities(aMedia:string);
    procedure SetDisplayPropertiesAndShow;
    property ScreenId : Integer read fScreenId write SetSCreenId;
    property MediasPath : String read FMediasPath write FMediasPath;
    property Medias : TStringList read fMedias;
    property MediaIndex : Integer read fMediaIndex write SetMediaIndex;
    property CurrentMedia : String read GetCurrentMedia;
    property CurrentMonitor : TMonitor read GetCurrentMonitor;
    property LockByPlayer : Boolean read fLockByPlayer write fLockByPlayer;
    property SkipOneTurn : Boolean read fSkipOneTurn write fSkipOneTurn;
    property isVideoEnabled : Boolean read GetisVideoEnabled;
    property Play : Boolean read fPlay write fPlay;
  end;

var
  frmDisplay: TfrmDisplay;

implementation

uses wndMain, UNew, unitplan, unitswitch, uniteditmonitor, uniteditpos, inifiles,
  ShlObj, UnitBrowseDirectory, uCommon, uBoardLiteCommon, wndProperties, StrUtils;

{$R *.dfm}


{ TfrmDisplay }

procedure TfrmDisplay.StopMedias();
begin
  StopVideo();
end;

procedure TfrmDisplay.FormCreate(Sender: TObject);
begin
  Self.Color:=clBlack;
  fMedias := TStringList.Create();
  DoubleBuffered := True;
end;

procedure TfrmDisplay.FormClose(Sender: TObject; var Action: TCloseAction);
  procedure DeleteTemporaryMedias;
  var
    nCount : Integer;
    TmpMedias : TStringList;
  begin
    TmpMedias := TStringList.Create;
    GetAllFilesFromMask(MediasPath, TmpMedias);
    for nCount := 0 to TmpMedias.Count-1 do
      if IsFileDeleted(TmpMedias[nCount]) then
        RenameFile(TmpMedias[nCount], ReplaceString(TmpMedias[nCount], CST_DELETED_EXTENSION, ''))
      else if IsFileAdded(TmpMedias[nCount]) then
        DeleteFile(TmpMedias[nCount]);
    TmpMedias.Free();
  end;
begin
  if not (fLastThread = nil) then
  begin
    fLastThread.Terminate;
    hDspBoardLog('Free Form (Last Thread ID' + IntToStr(fLastThread.Handle) + ')');
    fLastThread := nil;
  end;
  StopMedias();
  DeleteTemporaryMedias();
  fMedias.Free();
//  Application.ProcessMessages();
  Action:=caFree;
end;

procedure TfrmDisplay.DoCloseMain();
begin
  frmMain.Close();
end;

procedure TfrmDisplay.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  Position:TPoint;
begin
  GetCursorPos(Position);
  { TODO : KEY }
  case key of
    27: (*Echap*)
      if (ssCtrl in Shift) then
        DoCloseMain()
      else
      begin
//        frmMain.VisibleCursor:=not(frmMain.VisibleCursor);
//        frmMain.DisplayEditMode(frmMain.VisibleCursor);
//        frmMain.TimersActivated:=not(frmMain.VisibleCursor);
      end;
    VK_F4:
      if (ssAlt in Shift) then DoCloseMain()
  end;
end;

procedure TfrmDisplay.Newitem1Click(Sender: TObject);
var
  ObjList:TStringList;
  LabelId:string;
  LabelCaption:string;
  LabelScreen:string;
  LabelLeft:string;
  LabelTop:string;
  s:string;
begin
  if frmMain.VisibleCursor then
  begin
    ObjList:=TStringList.Create();
    ObjList.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'item.csv');
    s:=ObjList[ObjList.Count-1];
    LabelId:=GetCSVcell(s, COL_ID);
    try
      LabelId:=IntToStr(StrToInt(LabelId)+1);
    except
      On E:Exception do
      begin
        hDspBoardLog(E);
        LabelId:='1';
        s:=SetCSVcell(s, COL_FONTNAME, 'Arial');
        s:=SetCSVcell(s, COL_FONTSIZE, '14');
        s:=SetCSVcell(s, COL_BOLD, '0');
        s:=SetCSVcell(s, COL_COLOR, '#FFFFFF');
      end;
    end;
    labelCaption:='Double-Click to modify';
    LabelScreen:=IntToStr(ScreenId);
    LabelLeft:='0';
    LabelTop:='0';
    s:=SetCSVcell(s, COL_ID, LabelId);
    s:=SetCSVcell(s, COL_CAPTION, LabelCaption);
    s:=SetCSVcell(s, COL_SCREEN, LabelScreen);
    s:=SetCSVcell(s, COL_SCREENWIDTH, IntToStr(CurrentMonitor.Width));
    s:=SetCSVcell(s, COL_SCREENHEIGHT, IntToStr(CurrentMonitor.Height));
    s:=SetCSVcell(s, COL_LEFT, LabelLeft);
    s:=SetCSVcell(s, COL_TOP, LabelTop);
    ObjList.Add(s);
    SaveStringsToCSV(ExtractFilePath(Application.Exename)+'item.csv',ObjList.Text);
    frmMain.LoadPgms();
    ObjList.Free();
  end;
end;

procedure TfrmDisplay.Quit1Click(Sender: TObject);
begin
  DoCloseMain();
end;

procedure TfrmDisplay.Schedulefolders1Click(Sender: TObject);
begin
  frmMain.TimersActivated:=false;
  FormPlan:=TFormPlan.Create(Self);
  FormPlan.ShowModal();
  FormPlan.Release();
  frmMain.TimersActivated:=true;
end;

procedure TfrmDisplay.mnuDisplayScreenClick(Sender: TObject);
var
  i:integer;
  s:string;
begin
  s:='';
  for i:=0 to Screen.MonitorCount-1 do
  begin
    s:=s+
      IntToStr(Screen.Monitors[i].MonitorNum+1)+' => '
      +'[X:'+IntToStr(Screen.Monitors[i].Left)+';'
      +'Y:'+IntToStr(Screen.Monitors[i].Top)+']'
      +' (W:'+IntToStr(Screen.Monitors[i].Width)+';'
      +'H:'+IntToStr(Screen.Monitors[i].Height)+')'
      +#13#10;
  end;
  ShowMessage(s);
end;

procedure TfrmDisplay.mnuSwitchScreensClick(Sender: TObject);
begin
  FormSwitch:=TFormSwitch.Create(Self);
  FormSwitch.ShowModal();
  FormSwitch.Release();
end;

procedure TfrmDisplay.mnuRefreshScreenClick(Sender: TObject);
begin
  frmMain.LoadPgms();
end;

procedure TfrmDisplay.mnuEditMonitorsClick(Sender: TObject);
begin
  FormEditMonitor:=TFormEditMonitor.Create(Self);
  FormEditMonitor.ShowModal();
  FormEditMonitor.Free();
  frmMain.LoadPgms();
end;

procedure TfrmDisplay.mnuEditProductsClick(Sender: TObject);
begin
  FormEditPos:=TFormEditPos.Create(Self);
  FormEditPos.ShowModal();
  FormEditPos.Free();
  frmMain.LoadPgms();
end;

procedure TfrmDisplay.mnuGridClick(Sender: TObject);
begin
  mnuGrid.Checked:=not(mnuGrid.Checked);
  frmMain.AlignToGrid:=mnuGrid.Checked;
end;

procedure TfrmDisplay.PopupMenuFormPopup(Sender: TObject);
begin
  mnuGrid.Checked:=frmMain.AlignToGrid;
end;

procedure TfrmDisplay.TimerMediaRotatorTimer(Sender: TObject);
begin
  PlayNextMedia();
end;

function TfrmDisplay.IsFileDeleted(_FileName : String) : Boolean;
begin
  Result := RightStr(ExtractOnlyFileName(_FileName), Length(CST_DELETED_EXTENSION)) = CST_DELETED_EXTENSION;
end;

function TfrmDisplay.IsFileAdded(_FileName : String) : Boolean;
begin
  Result := RightStr(ExtractOnlyFileName(_FileName), Length(CST_ADDED_EXTENSION)) = CST_ADDED_EXTENSION;
end;

Procedure TfrmDisplay.LoadMedia();
var
  MonitorCSV:TStringList;
  sFileName : String;
  nCount : Integer;
  lRandom : Boolean;
begin

  sFileName := ExtractFilePath(Application.Exename)+'monitor.csv';

  if FileExists(sFileName) then
  begin
    fMedias.Clear();
    MonitorCSV := TStringList.Create();
    MonitorCSV.LoadFromFile(sFileName);
    if (ScreenId > 0) and (ScreenId < MonitorCSV.Count) then
    begin
      MediasPath := RelToAbs(GetCSVcell(MonitorCSV[ScreenId], 1), ExtractFilePath(Application.ExeName));
      lRandom := (GetCSVcell(MonitorCSV[ScreenId], 2) = 'Rnd');
      hDspBoardLog('Search files in ' + MediasPath);
      GetAllFilesFromMask(MediasPath, Medias);
      if lRandom then
        for nCount := Medias.Count-1 downto 1 do
          Medias.Exchange(nCount, Random(nCount+1));
      hDspBoardLog('Found ' + IntToStr(Medias.Count) + ' Items');

      for nCount := Medias.Count-1 downto 0 do
        if IsFileDeleted(Medias[nCount]) then Medias.Delete(nCount);

      if Medias.Count = 0 then
        hDspBoardLog('No media for this screen ('+IntToStr(ScreenId)+') !');

      fMediaIndex := -1;
      fStretchMode:=GetCSVcell(MonitorCSV[ScreenId], 2);
    end
    else
      hDspBoardLog('No media list for this screen ('+IntToStr(ScreenId)+')');
    MonitorCSV.Free();
  end;

end;

procedure TfrmDisplay.PlayNextMedia();
var
  lOldMediaWasVideo : Boolean;
begin
  try
    hDspBoardLog('Play next media');
    if SkipOneTurn then hDspBoardLog('Skiping one turn');
    if not LockByPlayer and not SkipOneTurn then
    begin
      lOldMediaWasVideo := not (MediaType(CurrentMedia) = mtPicture) and not (MediaType(CurrentMedia) = mtUndefined);
      if fMediaIndex+1 > fMedias.Count-1 then LoadMedia();
      fMediaIndex := fMediaIndex + 1;
      if (fMediaIndex <= fMedias.Count-1) then
        if FileExists(fMedias[fMediaIndex]) then
        begin
          hDspBoardLog('Detect Media : ' + fMedias[fMediaIndex]);
          Case MediaType(fMedias[fMediaIndex]) of
            mtVideo :   PlayVideo(fMedias[fMediaIndex]);
            mtPicture :
            begin
              SkipOneTurn := lOldMediaWasVideo;
              DisplayMedia(fMedias[fMediaIndex]);
            end;
            mtUndefined : DisplayMedia();
          end;
        end
        else
        begin
          hDspBoardLog('File ' + fMedias[fMediaIndex] + ' Could not be found');
          PlayNextMedia();
  //        Hide();
        end;
    end
    else
      SkipOneTurn := False;
  except
    On E:Exception do hDspBoardLog(E.Message);
  end;
end;

procedure TfrmDisplay.ResizeUponMonitor();
var
  iCurrDisplayCount, iNewDisplayCount : LongInt;
  pMonitorFromWinProc  : TMonitorFromWindow;
begin

  // Force monitor update, fix bug in customform, won't update at display change.
  // This a hack/cheat to multimon MonitorFromWindow func, it's fakes the result.
  // This is required to tell customform.getMonitor() to update the TScreen object.
  iCurrDisplayCount:=Screen.MonitorCount;
  pMonitorFromWinProc:=MonitorFromWindow;      // Backup pointer to dynamic assigned DLL func
  MonitorFromWindow:=cheatMonitorFromWindow;   // Assign cheat func
  monitor;                                     // call the monitor property that calls customform.getMonitor and cheatfunc
  MonitorFromWindow:=pMonitorFromWinProc;      // restore the original func
  // ==========
  iNewDisplayCount:=Screen.MonitorCount;
  try
    hDspBoardLog(
      'Assigning Screen : ' +
        IntToStr(CurrentMonitor.MonitorNum) + ' ' +
      'values (' +
        IntToStr(CurrentMonitor.Top) + ', ' +
        IntToStr(CurrentMonitor.Left) + ', ' +
        IntToStr(CurrentMonitor.Height) + ', ' +
        IntToStr(CurrentMonitor.Width) + ')'
        );
    hDspBoardLog('Force Visible');
    Visible := True;
    hDspBoardLog('Positioning');
    Left   := CurrentMonitor.Left;
    Top    := CurrentMonitor.Top;
    Width  := CurrentMonitor.Width;
    Height := CurrentMonitor.Height;
    hDspBoardLog('Force Foreground Window');
    ForceForegroundWindow(Handle);
    hDspBoardLog('End Assigning');
  except
    On E:Exception do
    begin
      hDspBoardLog(E);
      Visible := False;
    end;
  end;
  if( iCurrDisplayCount <> iNewDisplayCount ) then
  begin
    try
      hDspBoardLog('Reload all screen upon Monitors modifications');
      frmMain.LoadPgms();
    except
      On E:Exception do
      begin
        hDspBoardLog(E);
        frmMain.RestartApplication();
      end;
    end
  end;
end;

procedure TfrmDisplay.PlayVideo(_MediaFileName : String);
var
  CommandLine : String;
  ProcessInformation : TProcessInformation;
  rctPanel : TRect;
begin
  if not fPlay then
  begin
    if frmMain.ExternalMediaPlayer then
    begin
      if isVideoEnabled then
      begin
        CommandLine := ReplaceString(frmMain.VideoPlayer_CommandLine, '%MediaFileName%', _MediaFileName);
        CommandLine := ReplaceString(CommandLine, '%MonitorNo%', IntToStr(ScreenId));
        if StartProcess(CommandLine, CurrentMonitor, ProcessInformation) then
        begin
          hDspBoardLog('Playing ' + CommandLine);
          LockByPlayer := True;
          fLastThread := TEndProcessThread.Create(ProcessInformation.hProcess, EndVideo, Self);
          hDspBoardLog('Playing Thread ID' + IntToStr(fLastThread.Handle) + ')');
        end
        else
        begin
          hDspBoardLog('Could not play Video, Disable Video player !');
          frmMain.VideoPlayer_CommandLine := '';
        end;
        fProcessToTerminate := ProcessInformation.hProcess;
      end;
    end
    else
    begin
      mpVideo.FileName := _MediaFileName;
      mpVideo.Open;
      rctPanel.Left:=0;
      rctPanel.Right:=Width;
      rctPanel.Top:=0;
      rctPanel.Bottom:=Height;
      Stretch(mpVideo.DisplayRect, rctPanel);
      pnlVideo.Top:=rctPanel.Top;
      pnlVideo.Left:=rctPanel.Left;
      pnlVideo.Width:=rctPanel.Right;
      pnlVideo.Height:=rctPanel.Bottom;
      mpVideo.DisplayRect:=pnlVideo.ClientRect;
      pnlVideo.Visible := True;
      pnlVideo.BringToFront();
      mpVideo.Notify := True;
      mpVideo.Play;
    end;
    fPlay := True;
  end;
end;

procedure TfrmDisplay.EndVideo(Sender: TObject);
begin
  fPlay := False;
  if isVideoEnabled and not (fLastThread = nil) then
  begin
    hDspBoardLog('End video (Last Thread ID' + IntToStr(fLastThread.Handle) + ')');
    LockByPlayer := False;
    PlayNextMedia();
  end
  else
    hDspBoardLog('Last Thread ID is nil');
end;

procedure TfrmDisplay.DisplayMedia(_MediaFileName : String = '');
var
  Pict:TPicture;
  PictWidth:integer;
  PictHeight:integer;
  ImageWidth:integer;
  ImageHeight:integer;
  ImageLeft:integer;
  ImageTop:integer;
  ImageRatio:double;
  lLoaded : Boolean;

  procedure PrepareImage(_ImageToDisplay : TImage);
  begin
    _ImageToDisplay.Left:=frmMain.ForceGridPosition(ImageLeft);
    _ImageToDisplay.Top:=frmMain.ForceGridPosition(ImageTop);
    _ImageToDisplay.Picture:=Pict;
    _ImageToDisplay.Width:=ImageWidth;
    _ImageToDisplay.Height:=ImageHeight;
  end;

begin

  pnlVideo.Visible := False;

  if FileExists(_MediaFileName) then
  begin
    hDspBoardLog('Display media : ' + _MediaFileName);

    Pict:=TPicture.Create;

    lLoaded := False;
    Try
      Pict.LoadFromFile(_MediaFileName);
      lLoaded := True;
    Except
      On E:Exception do hDspBoardLog(E);
    End;

    if lLoaded then
    begin

      PictWidth:=Pict.Width;
      PictHeight:=Pict.Height;

      ImageLeft:=0;
      ImageTop:=0;
      if fStretchMode='+H' then
      begin
        ImageRatio:=PictWidth/PictHeight;
        ImageHeight:=Height;
        ImageWidth:=Round(ImageHeight*ImageRatio);
        if ImageWidth<>Width then
          ImageLeft:=(Width-ImageWidth)div 2;
      end else if fStretchMode='+W' then
      begin
        ImageRatio:=PictWidth/PictHeight;
        ImageWidth:=Width;
        ImageHeight:=Round(ImageWidth/ImageRatio);
        if ImageHeight<>Height then
          ImageTop:=(Height-ImageHeight)div 2;
      end
      else
      begin
        ImageWidth:=Width;
        ImageHeight:=Height;
      end;

      if imgMain1.Visible then
        PrepareImage(imgMain2)
      else
        PrepareImage(imgMain1);

    end;

    Pict.Free();
  end
  else if _MediaFileName = '' then
    DisplayEmptyScreen()
  else
    hDspBoardLog('Loading media : ' + _MediaFileName + ' failed. Not existing !');
end;

procedure TfrmDisplay.SetMainImage(_Image1 : Boolean);
begin
  if _Image1 then
  begin
    imgMain1.Visible := True;
    imgMain2.Visible := False;
    imgMain1.BringToFront();
  end
  else
  begin
    imgMain1.Visible := False;
    imgMain2.Visible := True;
    imgMain2.BringToFront();
  end;
  if frmMain.DisplayMonitor then lblMonitor.BringToFront();
end;

procedure TfrmDisplay.GoDisplay(_Force : Boolean = False);
begin
  if imgMain1.Visible or imgMain2.Visible or _Force then SetMainImage(not imgMain1.Visible);
  hDspBoardLog('Go Display');
  Show();
end;

procedure TfrmDisplay.DisplayEmptyScreen();
begin
  imgMain1.Left := 0;
  imgMain1.Top := 0;
  imgMain1.Width := Width;
  imgMain1.Height := Height;
  imgMain1.Canvas.Brush.Style := bsSolid;
  imgMain1.Canvas.Brush.Color := Color;
  imgMain1.Canvas.FillRect(imgMain1.BoundsRect);
  SetMainImage(True);
end;

procedure TfrmDisplay.DisplayItems;
var
  nCount:integer;
  ObjList:TStringList;
  isMobile:boolean;
  LabelId:string;
  LabelCaption:string;
  LabelLeft:string;
  LabelTop:string;
  LabelColor:string;
  LabelSize:string;
  LabelFont:string;
  LabelBold:string;
  LabelScreenWidth:string;
  LabelScreenHeight:string;
  Label1:TLabel;
  Marquee1:TRealTimeMarquee;
  MarqueeItem1:TMarqueeItem;
begin
  hDspBoardLog('DisplayItem');
  ObjList:=TStringList.Create();
  ObjList.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'item.csv');
      for nCount :=1 to ObjList.Count-1 do
      begin
        if GetCSVcell(ObjList[nCount], COL_SCREEN)=IntToStr(ScreenId) then
        begin
          isMobile:=(GetCSVcell(ObjList[nCount], COL_MOBILE)='1');
          LabelId:=GetCSVcell(ObjList[nCount], COL_ID);
          LabelCaption:=GetCSVcell(ObjList[nCount], COL_CAPTION);
          LabelScreenWidth:=GetCSVcell(ObjList[nCount], COL_SCREENWIDTH);
          LabelScreenHeight:=GetCSVcell(ObjList[nCount], COL_SCREENHEIGHT);
          LabelLeft:=GetCSVcell(ObjList[nCount], COL_LEFT);
          LabelTop:=GetCSVcell(ObjList[nCount], COL_TOP);
          LabelFont:=GetCSVcell(ObjList[nCount], COL_FONTNAME);
          LabelSize:=GetCSVcell(ObjList[nCount], COL_FONTSIZE);
          LabelBold:=GetCSVcell(ObjList[nCount], COL_BOLD);
          LabelColor:=GetCSVcell(ObjList[nCount], COL_COLOR);

          if (not isMobile) then
          begin
            Label1:=TLabel.Create(frmDisplay);
            Label1.Parent:=frmDisplay;
            Label1.Transparent:=true;
            Label1.Left:=Round(StrToInt(LabelLeft)/StrToInt(LabelScreenWidth)*CurrentMonitor.Width);
            Label1.Top:=Round(StrToInt(LabelTop)/StrToInt(LabelScreenHeight)*CurrentMonitor.Height);
            Label1.Tag:=StrToInt(LabelId);
            Label1.Caption:=CheckDynamicCaption(LabelCaption, CurrentMedia);
            if Copy(Label1.Caption, 1, 1) = '/' then
            begin
              Label1.Caption:=Copy(Label1.Caption, 2, Length(Label1.Caption)-1);
              Label1.Font.Style:=Label1.Font.Style+[fsStrikeOut];
            end;
            Label1.Font.Name:=LabelFont;
            if (LabelBold='1') or (LabelBold='3') then
              Label1.Font.Style:=Label1.Font.Style+[fsBold];
            if (LabelBold='2') or (LabelBold='3') then
              Label1.Font.Style:=Label1.Font.Style+[fsItalic];
            Label1.Font.Size:=
              Round(StrToInt(LabelSize)/StrToInt(LabelScreenHeight)*CurrentMonitor.Height);
            Label1.Font.Color:=HexToColor(LabelColor);
            Label1.OnDblClick:=DynamicLabelClick;
            Label1.OnMouseDown:=DynamicLabelMouseDown;
            Label1.OnMouseUp:=DynamicLabelMouseUp;
            Label1.OnMouseMove:=DynamicLabelMouseMove;
          end
          else
          begin
            Marquee1:=TRealTimeMarquee.Create(frmDisplay);
            Marquee1.Parent:=frmDisplay;
            Marquee1.Interval:=frmMain.Speed;
            Marquee1.Color:=clBlack;
            Marquee1.Left:=Round(StrToInt(LabelLeft)/StrToInt(LabelScreenWidth)*CurrentMonitor.Width);
            Marquee1.Top:=Round(StrToInt(LabelTop)/StrToInt(LabelScreenHeight)*CurrentMonitor.Height);
            Marquee1.Tag:=StrToInt(LabelId);
            MarqueeItem1:=Marquee1.Items.Add();
            MarqueeItem1.Text:=CheckDynamicCaption(LabelCaption, CurrentMedia);
            if Copy(MarqueeItem1.Text, 1, 1) = '/' then
            begin
              MarqueeItem1.Text:=Copy(MarqueeItem1.Text, 2, Length(MarqueeItem1.Text)-1);
              Marquee1.Font.Style:=Marquee1.Font.Style+[fsStrikeOut];
            end;
            Marquee1.Font.Name:=LabelFont;
            if (LabelBold='1') or (LabelBold='3') then
              Marquee1.Font.Style:=Marquee1.Font.Style+[fsBold];
            if (LabelBold='2') or (LabelBold='3') then
              Marquee1.Font.Style:=Marquee1.Font.Style+[fsItalic];
            Marquee1.Font.Size:=
              Round(StrToInt(LabelSize)/StrToInt(LabelScreenHeight)*CurrentMonitor.Height);
            Marquee1.Font.Color:=HexToColor(LabelColor);

            // ETAF3733
            Marquee1.Left:=0;
            Marquee1.Width:=CurrentMonitor.Width;
            //Marquee1.Orientation:=moVertical;
            //Marquee1.Spacing:=50;
            Marquee1.Height:=Marquee1.Font.Size*3;

            Marquee1.OnDblClick:=DynamicLabelClick;
            Marquee1.OnMouseDown:=DynamicLabelMouseDown;
            Marquee1.OnMouseUp:=DynamicLabelMouseUp;
            Marquee1.OnMouseMove:=DynamicLabelMouseMove;

            // ETAF3876
            Marquee1.Reset;
            Marquee1.Active:=true;
          end;
        end;
      end;
end;

procedure TfrmDisplay.X1Click(Sender: TObject);
var
  j:integer;
begin
  for j:=Self.ControlCount-1 downto 0 do
  begin
    if (Self.Controls[j] is TRealTimeMarquee) then
    begin
      (Self.Controls[j] as TRealTimeMarquee).Reset;
    end;
  end;
end;

procedure TfrmDisplay.mnuSaveSchemeClick(Sender: TObject);
var
  folder:string;
begin
  // http://msdn.microsoft.com/en-us/library/bb773205%28v=vs.85%29.aspx
  folder:=BrowseDialog('Please select the directory to save configuration'
    ,BIF_STATUSTEXT
    or BIF_NEWDIALOGSTYLE
    , ExtractFilePath(Application.Exename)
  );
  if folder<>'' then
  begin
    CopyFile(PAnsiChar(ExtractFilePath(Application.Exename)+'hboardlite.ini')
      , PAnsiChar(folder+'\'+'hboardlite.ini'), false);
    CopyFile(PAnsiChar(ExtractFilePath(Application.Exename)+'hboardlite.csv')
      , PAnsiChar(folder+'\'+'hboardlite.csv'), false);
    CopyFile(PAnsiChar(ExtractFilePath(Application.Exename)+'item.csv')
      , PAnsiChar(folder+'\'+'item.csv'), false);
    CopyFile(PAnsiChar(ExtractFilePath(Application.Exename)+'monitor.csv')
      , PAnsiChar(folder+'\'+'monitor.csv'), false);
  end;
end;

procedure TfrmDisplay.mnuLoadSchemeClick(Sender: TObject);
var
  folder:string;
begin
  // http://msdn.microsoft.com/en-us/library/bb773205%28v=vs.85%29.aspx
  folder:=BrowseDialog('Please select the directory to load configuration'
    ,BIF_STATUSTEXT
    or BIF_NEWDIALOGSTYLE
    or $200
    , ExtractFilePath(Application.Exename)
  );
  if folder<>'' then
  begin
    CopyFile(PAnsiChar(folder+'\'+'hboardlite.ini')
      , PAnsiChar(ExtractFilePath(Application.Exename)+'hboardlite.ini'), false);
    CopyFile(PAnsiChar(folder+'\'+'hboardlite.csv')
      , PAnsiChar(ExtractFilePath(Application.Exename)+'hboardlite.csv'), false);
    CopyFile(PAnsiChar(folder+'\'+'item.csv')
      , PAnsiChar(ExtractFilePath(Application.Exename)+'item.csv'), false);
    CopyFile(PAnsiChar(folder+'\'+'monitor.csv')
      , PAnsiChar(ExtractFilePath(Application.Exename)+'monitor.csv'), false);
    frmMain.LoadPgms();
  end;
end;

procedure TfrmDisplay.CheckVisibilities(aMedia:string);
var
  i:integer;
  nLine:integer;
  o:TLabel;
  LabelId:string;
  ObjList:TStringList;
  LabelCaption:string;
  LabelReserved:string;
begin
  ObjList:=TStringList.Create();
  ObjList.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'item.csv');

  for i:=0 to Self.ControlCount-1 do
  begin
    if (Self.Controls[i] is TLabel) then
    begin
      o:=(Self.Controls[i] as TLabel);
      LabelId:=IntToStr(o.Tag);

      // Recherche de la ligne
      nLine:=0;
      while (nLine<=ObjList.Count-1)
        and (GetCSVcell(ObjList[nLine], COL_ID)<>LabelId) do
      begin
        nLine:=nLine+1;
      end;

      if (nLine<=ObjList.Count-1) then
      begin
        LabelCaption:=GetCSVcell(ObjList[nLine], COL_CAPTION);
        LabelReserved:=GetCSVcell(ObjList[nLine], COL_RESERVED);
        if (LabelReserved <> '') then
        begin
          o.Visible:=ExtractFileName(aMedia)=LabelReserved;
        end;
      end;
    end;
  end;
  ObjList.Free();
end;

function ForceRestoreWindow(WndHandle: HWND; Immediate: Boolean): Boolean;
var
  WindowPlacement: TWindowPlacement;
begin
  Result := False;
  if Immediate then
  begin
    WindowPlacement.length := SizeOf(WindowPlacement);
    if GetWindowPlacement(WndHandle, @WindowPlacement) then
    begin
      if (WindowPlacement.flags and WPF_RESTORETOMAXIMIZED) <> 0 then
        WindowPlacement.showCmd := SW_MAXIMIZE
      else
        WindowPlacement.showCmd := SW_RESTORE;
      Result := SetWindowPlacement(WndHandle, @WindowPlacement);
    end;
  end
  else
    Result := SendMessage(WndHandle, WM_SYSCOMMAND, SC_RESTORE, 0) = 0;
end;

procedure TfrmDisplay.WMDeviceChange(var Msg: TMessage);
begin
  ResizeUponMonitor();
//  frmMain.LoadPgms();
end;

procedure TfrmDisplay.StopVideo();
begin
  if frmMain.ExternalMediaPlayer then
  begin
    if not(fProcessToTerminate = 0) then
    begin
      hDspBoardLog('Killing process');
      KillProcess(fProcessToTerminate);
      fProcessToTerminate := 0;
    end;
  end
  else
  begin
    try
      mpVideo.Stop;
    except
//      hDspBoardLog('Could not stop player')
    end;
    try
      mpVideo.Close;
    except
//      hDspBoardLog('Could not close player')
    end;
  end;
  fPlay := False;
end;

procedure TfrmDisplay.SetSCreenId(const Value: Integer);
begin
  fScreenId := Value;
  SetDisplayPropertiesAndShow;
  Tag := fScreenId;
  LoadMedia();
  lblMonitor.Visible := frmMain.DisplayMonitor;
  lblMonitor.Caption := IntToStr(fScreenId);
end;

function TfrmDisplay.GetCurrentMonitor : TMonitor;
begin
  if (fScreenId-1 >= 0) and (fScreenId-1 <= Screen.MonitorCount-1) then
    Result := Screen.Monitors[fScreenId-1]
  else
    Result := nil;
end;

procedure TfrmDisplay.SetDisplayPropertiesAndShow();
begin
  hDspBoardLog('SetDisplayPropertiesAndShow');
  ResizeUponMonitor();
  CheckVisibilities(CurrentMedia);
  LabelEditMode.Left:=Width-LabelEditMode.Width-16;
end;

procedure TfrmDisplay.SetMediaIndex(const Value: Integer);
begin
  fMediaIndex := Value;
end;

function TfrmDisplay.GetCurrentMedia: String;
begin
  if (fMediaIndex >= 0) and (fMediaIndex <= fMedias.Count-1) then
    Result := fMedias[fMediaIndex]
  else
    Result := '';
end;

procedure TfrmDisplay.DynamicLabelClick(Sender: TObject);
var
  ObjList:TStringList;
  o:TLabel;
  o2:TRealTimeMarquee;
  LabelId:string;
  LabelCaption:string;
  LabelScreen:string;
  LabelWidth:string;
  LabelHeight:string;
  LabelLeft:string;
  LabelTop:string;
  LabelFont:string;
  LabelSize:string;
  LabelBold:string;
  LabelColor:string;
  LabelMobile:string;
  LabelReserved:string;
  nLine:integer;
begin
  o:=nil;
  o2:=nil;
  if frmMain.VisibleCursor then
  begin
    if (Sender is TLabel) or (Sender is TRealTimeMarquee) then
    begin
      if (Sender is TLabel) then
      begin
        o:=(Sender as TLabel);
        LabelId:=IntToStr(o.Tag);
      end;
      if (Sender is TRealTimeMarquee) then
      begin
        o2:=(Sender as TRealTimeMarquee);
        LabelId:=IntToStr(o2.Tag);
      end;

      ObjList:=TStringList.Create();
      ObjList.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'item.csv');

      // Recherche de la ligne
      nLine:=0;
      while (nLine<=ObjList.Count-1)
        and (GetCSVcell(ObjList[nLine], COL_ID)<>LabelId) do
      begin
        nLine:=nLine+1;
      end;

      if (nLine<=ObjList.Count-1) then
      begin
        LabelCaption:=GetCSVcell(ObjList[nLine], COL_CAPTION);
        LabelScreen:=GetCSVcell(ObjList[nLine], COL_SCREEN);
        LabelWidth:=GetCSVcell(ObjList[nLine], COL_SCREENWIDTH);
        LabelHeight:=GetCSVcell(ObjList[nLine], COL_SCREENHEIGHT);

        LabelLeft:=GetCSVcell(ObjList[nLine], COL_LEFT);
        LabelTop:=GetCSVcell(ObjList[nLine], COL_TOP);

        if (Sender is TLabel) and not (o = nil)then
        begin
          LabelLeft:=IntToStr(o.left);
          LabelTop:=IntToStr(o.top);
        end;
        if (Sender is TRealTimeMarquee) and not (o2 = nil) then
        begin
          LabelLeft:=IntToStr(o2.left);
          LabelTop:=IntToStr(o2.top);
        end;

        LabelFont:=GetCSVcell(ObjList[nLine], COL_FONTNAME);
        LabelSize:=GetCSVcell(ObjList[nLine], COL_FONTSIZE);
        LabelBold:=GetCSVcell(ObjList[nLine], COL_BOLD);
        LabelColor:=GetCSVcell(ObjList[nLine], COL_COLOR);
        LabelMobile:=GetCSVcell(ObjList[nLine], COL_MOBILE);
        LabelReserved:=GetCSVcell(ObjList[nLine], COL_RESERVED);

        FormProperties:=TFormProperties.Create(Self);

        FormProperties.EditTitle.Text:=LabelCaption;
        if (copy(LabelCaption, 1, 1)='@') then
        begin
          if (copy(LabelCaption, 1, 3)='@SD') then
          begin
            FormProperties.ComboBoxType.ItemIndex:=1;
            FormProperties.EditTitle.Text:=copy(FormProperties.EditTitle.Text, 4, length(FormProperties.EditTitle.Text)-3);
          end
          else if (copy(LabelCaption, 1, 3)='@LD') then
          begin
            FormProperties.ComboBoxType.ItemIndex:=2;
            FormProperties.EditTitle.Text:=copy(FormProperties.EditTitle.Text, 4, length(FormProperties.EditTitle.Text)-3);
          end
          else if (copy(LabelCaption, 1, 3)='@P1') then
          begin
            FormProperties.ComboBoxType.ItemIndex:=3;
            FormProperties.EditTitle.Text:=copy(FormProperties.EditTitle.Text, 4, length(FormProperties.EditTitle.Text)-3);
          end
          else if (copy(LabelCaption, 1, 3)='@P2') then
          begin
            FormProperties.ComboBoxType.ItemIndex:=4;
            FormProperties.EditTitle.Text:=copy(FormProperties.EditTitle.Text, 4, length(FormProperties.EditTitle.Text)-3);
          end
          else if (copy(LabelCaption, 1, 3)='@P3') then
          begin
            FormProperties.ComboBoxType.ItemIndex:=5;
            FormProperties.EditTitle.Text:=copy(FormProperties.EditTitle.Text, 4, length(FormProperties.EditTitle.Text)-3);
          end
          else if (copy(LabelCaption, 1, 3)='@P4') then
          begin
            FormProperties.ComboBoxType.ItemIndex:=6;
            FormProperties.EditTitle.Text:=copy(FormProperties.EditTitle.Text, 4, length(FormProperties.EditTitle.Text)-3);
          end
          else if (copy(LabelCaption, 1, 3)='@P5') then
          begin
            FormProperties.ComboBoxType.ItemIndex:=7;
            FormProperties.EditTitle.Text:=copy(FormProperties.EditTitle.Text, 4, length(FormProperties.EditTitle.Text)-3);
          end;
        end
        else
        begin
          FormProperties.ComboBoxType.ItemIndex:=0;
        end;
        FormProperties.EditX.Text:=LabelLeft;
        FormProperties.EditY.Text:=LabelTop;
        FormProperties.EditReserved.Text:=LabelReserved;
        FormProperties.LabelFontName.Caption:=LabelFont;
        FormProperties.LabelFontSize.Caption:=LabelSize;
        FormProperties.CheckBoxBold.Checked:=
          (LabelBold='1') or (LabelBold='3');
        FormProperties.CheckBoxItalic.Checked:=
          (LabelBold='2') or (LabelBold='3');
        FormProperties.PanelPen.Color:=HexToColor(LabelColor);
        FormProperties.CheckBoxMobile.Checked:=
          (LabelMobile='1');

        FormProperties.ShowModal();
        if FormProperties.ModalResult=mrOk then
        begin
          LabelCaption:=FormProperties.EditTitle.Text;
          case (FormProperties.ComboBoxType.ItemIndex) of
            0: ;
            1: LabelCaption:='@SD'+LabelCaption;
            2: LabelCaption:='@LD'+LabelCaption;
            3: LabelCaption:='@P1'+LabelCaption;
            4: LabelCaption:='@P2'+LabelCaption;
            5: LabelCaption:='@P3'+LabelCaption;
            6: LabelCaption:='@P4'+LabelCaption;
            7: LabelCaption:='@P5'+LabelCaption;
          end;
          try
            LabelLeft:=IntToStr(frmMain.ForceGridPosition(StrToInt(FormProperties.EditX.Text)));
          except
            On E:Exception do
            begin
              hDspBoardLog(E);
              LabelLeft:=FormProperties.EditX.Text;
            end;
          end;
          try
            LabelTop:=IntToStr(frmMain.ForceGridPosition(StrToInt(FormProperties.EditY.Text)));
          except
            On E:Exception do
            begin
              hDspBoardLog(E);
              LabelTop:=FormProperties.EditY.Text;
            end;
          end;
          LabelFont:=FormProperties.LabelFontName.Caption;
          LabelSize:=FormProperties.LabelFontSize.Caption;
          LabelReserved:=FormProperties.EditReserved.Text;
          if (FormProperties.CheckBoxBold.Checked) then
          begin
            if (FormProperties.CheckBoxItalic.Checked) then
            begin
              LabelBold:='3';
            end
            else
            begin
              LabelBold:='1';
            end;
          end
          else
          begin
            if (FormProperties.CheckBoxItalic.Checked) then
            begin
              LabelBold:='2';
            end
            else
            begin
              LabelBold:='0';
            end;
          end;
          LabelColor:=ColorToHex(FormProperties.PanelPen.Color);
          if (FormProperties.CheckBoxMobile.Checked) then
          begin
            LabelMobile:='1';
          end
          else
          begin
            LabelMobile:='';
          end;

          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_CAPTION, LabelCaption);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_SCREEN, LabelScreen);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_SCREENWIDTH, LabelWidth);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_SCREENHEIGHT, LabelHeight);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_LEFT, LabelLeft);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_TOP, LabelTop);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_FONTNAME, LabelFont);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_FONTSIZE, LabelSize);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_BOLD, LabelBold);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_COLOR, LabelColor);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_MOBILE, LabelMobile);
          ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_RESERVED, LabelReserved);

          if (LabelCaption='') then
          begin
            ObjList.Delete(nLine);
          end;

          SaveStringsToCSV(ExtractFilePath(Application.Exename)+'item.csv',ObjList.Text);
          PostMessage(Handle, WM_USER, 0, 0); // Display boards
        end;
        FormProperties.Release();
      end;
      ObjList.Free();
    end;
  end;
  PostMessage(Handle,WM_USER+1,0,0);
end;

procedure TfrmDisplay.DynamicLabelMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  o:TLabel;
  o2:TRealTimeMarquee;
  nLine:integer;
  LabelId:string;
  LabelLeft:string;
  LabelTop:string;
  LabelParentTag:integer;
  LabelFontSize:string;
  ObjList:TStringList;
begin
  LabelParentTag := -1;
  if true then
  begin
    if frmMain.VisibleCursor then
    begin
      if frmMain.IsMoving then
      begin
        if (Sender is TLabel) or (Sender is TRealTimeMarquee) then
        begin
          if (Sender is TLabel) then
          begin
            o:=(Sender as TLabel);
            o.Left:=frmMain.ForceGridPosition(o.Left);
            o.Top:=frmMain.ForceGridPosition(o.Top);
            LabelId:=IntToStr(o.Tag);
            LabelLeft:=IntToStr(o.Left);
            LabelTop:=IntToStr(o.Top);
            LabelParentTag:=o.Parent.Tag;
            LabelFontSize:=IntToStr(o.Font.Size);
          end;
          if (Sender is TRealTimeMarquee) then
          begin
            o2:=(Sender as TRealTimeMarquee);
            o2.Left:=frmMain.ForceGridPosition(o2.Left);
            o2.Left:=0;
            o2.Top:=frmMain.ForceGridPosition(o2.Top);
            LabelId:=IntToStr(o2.Tag);
            LabelLeft:=IntToStr(o2.Left);
            LabelTop:=IntToStr(o2.Top);
            LabelParentTag:=o2.Parent.Tag;
            LabelFontSize:=IntToStr(o2.Font.Size);
          end;

          ObjList:=TStringList.Create();
          ObjList.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'item.csv');

          // Recherche de la ligne
          nLine:=0;
          while (nLine<=ObjList.Count-1)
            and (GetCSVcell(ObjList[nLine], COL_ID)<>LabelId) do
          begin
            nLine:=nLine+1;
          end;

          if (nLine<=ObjList.Count-1) and (LabelParentTag > -1) then
          begin
            ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_LEFT, LabelLeft);
            ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_TOP, LabelTop);
            ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_SCREENWIDTH, IntToStr(Screen.Monitors[LabelParentTag-1].Width));
            ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_SCREENHEIGHT, IntToStr(Screen.Monitors[LabelParentTag-1].Height));
            ObjList[nLine]:=SetCSVcell(ObjList[nLine], COL_FONTSIZE, (LabelFontSize));
            if AUTOSAVE then
            begin
              SaveStringsToCSV(ExtractFilePath(Application.Exename)+'item.csv',ObjList.Text);
            end;
          end;
          ObjList.Free();
          frmMain.IsMoving:=false;
          // PostMessage(Handle,WM_USER, 0, 0); // Display boards
        end;
      end;
    end;
  end;
end;

procedure TfrmDisplay.DynamicLabelMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  o:TLabel;
  o2:TRealTimeMarquee;
begin
  if frmMain.VisibleCursor then
  begin
    if frmMain.IsMoving then
    begin
      if Sender is TLabel then
      begin
        o:=(Sender as TLabel);
        o.left:=o.left+x-x0;
        o.top:=o.top+y-y0;
      end;
      if Sender is TRealTimeMarquee then
      begin
        o2:=(Sender as TRealTimeMarquee);
        o2.left:=o2.left+x-x0;
        o2.top:=o2.top+y-y0;
      end;
    end;
  end;
end;

procedure TfrmDisplay.DynamicLabelMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    if frmMain.VisibleCursor then
    begin
      if Sender is TLabel then
      begin
        x0:=x;
        y0:=y;
        frmMain.IsMoving:=true;
      end;
      if Sender is TRealTimeMarquee then
      begin
        x0:=x;
        y0:=y;
        frmMain.IsMoving:=true;
      end;
    end;
  end;
end;

procedure TfrmDisplay.hDspBoardLog(_String : String; _Exception : Boolean = False);
begin
  frmMain.hBoardLog(_String + ' (Screen : '+IntToStr(ScreenId)+')', _Exception);
end;

procedure TfrmDisplay.hDspBoardLog(_Exception : Exception; _ComplementInfo : String = '');
begin
  frmMain.hBoardLog(_Exception, _ComplementInfo + ' (Screen : '+IntToStr(ScreenId)+')');
end;

function TfrmDisplay.GetisVideoEnabled: Boolean;
begin
  result := not (Trim(frmMain.VideoPlayer_CommandLine) = '');
end;

procedure TfrmDisplay.mpVideoNotify(Sender: TObject);
begin
  if mpVideo.NotifyValue=nvSuccessful then
  begin
    fPlay := False;
    PlayNextMedia;
  end;
end;

end.

