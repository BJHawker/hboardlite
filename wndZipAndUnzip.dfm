object frmZipAndUnzip: TfrmZipAndUnzip
  Left = 472
  Top = 292
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Veuillez patienter SVP !'
  ClientHeight = 92
  ClientWidth = 581
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    581
    92)
  PixelsPerInch = 96
  TextHeight = 20
  object lblMain: TLabel
    Left = 4
    Top = 4
    Width = 573
    Height = 21
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 'Sauvegarde en cours, veuillez patienter'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblMessage: TLabel
    Left = 4
    Top = 28
    Width = 573
    Height = 21
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 'Sauvegarde en cours, veuillez patienter'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object AbMeter1: TAbMeter
    Left = 4
    Top = 52
    Width = 573
    Height = 16
    Orientation = moHorizontal
    UnusedColor = clBtnFace
    UsedColor = clNavy
  end
  object AbMeter2: TAbMeter
    Left = 4
    Top = 72
    Width = 573
    Height = 16
    Orientation = moHorizontal
    UnusedColor = clBtnFace
    UsedColor = clNavy
  end
  object tmrBlink: TTimer
    Interval = 300
    OnTimer = tmrBlinkTimer
    Left = 348
    Top = 12
  end
  object UnZipper: TAbUnZipper
    ArchiveProgressMeter = AbMeter1
    ItemProgressMeter = AbMeter2
    ExtractOptions = [eoCreateDirs, eoRestorePath]
    OnProcessItemFailure = UnZipperProcessItemFailure
    Left = 24
    Top = 12
  end
  object Zipper: TAbZipper
    ArchiveSaveProgressMeter = AbMeter1
    ItemProgressMeter = AbMeter2
    AutoSave = True
    DOSMode = False
    OnProcessItemFailure = ZipperProcessItemFailure
    StoreOptions = [soRemoveDots, soRecurse]
    Left = 68
    Top = 12
  end
end
