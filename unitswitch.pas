unit unitswitch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TFormSwitch = class(TForm)
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure RadioGroupClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FormSwitch: TFormSwitch;

implementation

uses UNew, wndMain, Inifiles;

{$R *.dfm}

procedure TFormSwitch.btnOkClick(Sender: TObject);
var
  ObjList:TStringList;
  j:integer;
  Screen1,Screen2:string;
  Media1,Media2:string;
  ini:TIniFile;
begin
  if RadioGroup1.ItemIndex<>RadioGroup2.ItemIndex then
  begin
    Screen1:=Copy(RadioGroup1.Items[RadioGroup1.ItemIndex], 1, 1);
    Screen2:=Copy(RadioGroup2.Items[RadioGroup2.ItemIndex], 1, 1);
    ObjList:=TStringList.Create();
    ObjList.Text:=LoadCSVinStrings(ExtractFilePath(Application.Exename)+'item.csv');
    for j:=1 to ObjList.Count-1 do
    begin
      if GetCSVcell(ObjList[j], COL_SCREEN)=Screen1 then
      begin
        ObjList[j]:=SetCSVcell(ObjList[j], COL_SCREEN, Screen2);
      end
      else if GetCSVcell(ObjList[j], COL_SCREEN)=Screen2 then
      begin
        ObjList[j]:=SetCSVcell(ObjList[j], COL_SCREEN, Screen1);
      end;
    end;

    ini:=TIniFile.Create(ExtractFilePath(Application.Exename)+'hboardlite.ini');
    Media1:=ini.ReadString('config','monitor'+Screen1,'');
    Media2:=ini.ReadString('config','monitor'+Screen2,'');
    ini.DeleteKey('config','monitor'+Screen1);
    ini.DeleteKey('config','monitor'+Screen2);
    ini.WriteString('config','monitor'+Screen1,Media2);
    ini.WriteString('config','monitor'+Screen2,Media1);
    ini.Free();

    SaveStringsToCSV(ExtractFilePath(Application.Exename)+'item.csv', ObjList.Text);
    ObjList.Free();
    frmMain.LoadPgms();
    ModalResult:=mrOk;
  end;
end;

procedure TFormSwitch.RadioGroupClick(Sender: TObject);
begin
  btnOk.Enabled:=(RadioGroup1.ItemIndex<>RadioGroup2.ItemIndex);
end;

procedure TFormSwitch.FormCreate(Sender: TObject);
var
  i,j:integer;
  s:string;
begin
  for i:=0 to Screen.MonitorCount-1 do
  begin
    s:=
      IntToStr(Screen.Monitors[i].MonitorNum+1)+' => '
      +'[X:'+IntToStr(Screen.Monitors[i].Left)+';'
      +'Y:'+IntToStr(Screen.Monitors[i].Top)+']'
      +' (W:'+IntToStr(Screen.Monitors[i].Width)+';'
      +'H:'+IntToStr(Screen.Monitors[i].Height)+')'
    ;
    j:=RadioGroup1.Items.IndexOf(IntToStr(i+1));
    if j>-1 then
    begin
      RadioGroup1.Items[j]:=s;
      RadioGroup2.Items[j]:=s;
    end;
  end;

end;

end.
