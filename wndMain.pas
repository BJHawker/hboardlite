unit wndMain;

// v3.5.9 - 2017/05/07
//  Rendre possible la t�l�commande (Ajout/Suppresion de fichiers)
// v3.5.9.1 - 2017/05/21
//  Correction du pb d'affichage sur �cran en cas de coupure de courant
// v3.5.9.2 - 2017/06/06
//  Correction am�lioration du hBoardRemote
// v3.5.9.3 - 2017/06/11
//  Bug hBoardRemote et tentative r�solution EOutOfRessources (AutoRestart)
// v3.5.9.4 - 2017/10/15
//  Bug relatif � la pr�sence d'un fichier .sav sur une m�me date.
// v3.5.9.5 - 2017/10/15
//  Bug Affichage Ecran.
// v3.5.9.6 - 2017/10/30
//  Blocage du reload (Property LockLoading) en cas de modification d'�cran (Bug vraisemblablement initi� dans la 9.5.9.4
// v3.5.9.7 - 2017/11/02
//  D�tection d'un lock de fichier lors des chargements de nouvelles programmations
// v3.6.0.0 - 2017/11/17
//  Suppression du scintillement
// v3.6.1.0 - 2018/01/28
//  Rajout du GIF
// v3.6.2.0 - 2018/01/28
//  Correctif Video
// v3.6.2.1 - 2018/01/28
//  Debug Video
// v3.6.3.0 - 2018/02/04
//  Remise en place du TMediaPlayer
// v3.6.3.1 - 2018/02/05
//  Desactivation de la  touche ESC
// v3.6.3.2 - 2018/02/13
//  PBM de logs sur TMediaPlayer Stop. Rajout de l'heure...
// v3.6.3.3 - 2018/03/04
//  Restart sur chaque changement (Plan/Pgms)
// v3.6.3.4 - 2018/06/05
//  Prise en compte des MP4
// v3.6.3.5 - 2018/08/05
//  Interception de l'exception EList Error
// v3.6.3.6 - 2018/09/04
//  Debugging AllwaysOnTop et Restart Minimized
// v3.6.3.7 - 2018/09/05
//  Extinction du Timer lors de l'arr�t de l'application
// v3.6.3.8 - 2018/09/09
//  Reboot plusieurs fois par jour.
//    Utilisation de Lib7 1.1
// v3.6.3.9 - 2018/09/10
//  Reboot on Exception

interface

uses
  Windows, Messages, SysUtils, Variants,
  Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, JPeg,
  StdCtrls, CoolTrayIcon,
  Menus, DB, DBClient, Midaslib, RealTimeMarquee, VersInfo;

const
  AUTOSAVE = true;

  // CSV Structure
  COL_ID = 0;
  COL_CAPTION = 1;
  COL_SCREEN = 2;
  COL_SCREENWIDTH = 3;
  COL_SCREENHEIGHT = 4;
  COL_LEFT = 5;
  COL_TOP = 6;
  COL_FONTNAME = 7;
  COL_FONTSIZE = 8;
  COL_BOLD = 9;
  COL_COLOR = 10;
  COL_MOBILE = 11;
  COL_RESERVED = 12;

  CST_ARCHIVE_EXT = '.zip';
  CST_ARCHIVE_KO_EXT = '.ko';
  CST_LOG_EXT = '.log';
  CST_CONFIG_INI_FILENAME = 'Config.ini';

  CST_FORCEPLAN_FILENAME = 'ForcePlan.txt';
  CST_NEW_PARAMETERS_FILENAME = 'Config.ini';
  CST_NO_DOWNLOAD_FILENAME = 'NoDownload.txt';

  CST_CONFIG_SECTION = 'Config';
  CST_ROTATE_DELAY = 'rotate';
  CST_BACKUP = 'Backup';
  CST_DISPLAY_ZIP_PROGRESS = 'DisplayArchiveProgression';
  CST_VISIBLE_CURSOR = 'VisibleCursor';
  CST_EXT_MEDIA_PLAYER = 'ExternalMediaPlayer';
  CST_LOG_RETRY_ON_32 = 'LogRetryOn32';
  CST_RESTART_ON_NEW_PLANS = 'RestartOnNewPlans';
  CST_RESTART_ON_NEW_PGMS = 'RestartOnNewPgms';
  CST_RESTART_ON_EXCEPTION = 'RestartOnError';
  CST_REBOOT_ON_EXCEPTION = 'RebootOnException';
  CST_LOG_FILENAME = 'LogFile';
  CST_APPLICATION_LOG = 'ApplicationLog';
  CST_LOG_WINDOW = 'LogWindow';
  CST_SLEEP_AVAILABLE = 'SleepAvailable';
  CST_DISPLAY_MONITOR = 'DisplayMonitor';

  CST_SLEEP_PLAN = 'Veille';
  CST_DELETED_EXTENSION = '.DELETED';
  CST_ADDED_EXTENSION = '.ADDED';

type

  TfrmMain = class(TForm)
    TimerCheckNewPgms: TTimer;
    CoolTrayIcon1: TCoolTrayIcon;
    PopupMenuTray: TPopupMenu;
    Quitter1: TMenuItem;
    cdsPlan: TClientDataSet;
    cdsPlanday: TStringField;
    cdsPlandaylib: TStringField;
    cdsPlanhour: TStringField;
    cdsPlanfolder: TStringField;
    cdsDay: TClientDataSet;
    cdsDayday: TIntegerField;
    cdsDaylib: TStringField;
    TimerRotateMultiPlan: TTimer;
    TimerPowerMonitor: TTimer;
    dfsVersionInfoResource1: TdfsVersionInfoResource;
    TimerMediaRotator: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure TimerCheckNewPgmsTimer(Sender: TObject);
    procedure Quitter1Click(Sender: TObject);
    procedure CoolTrayIcon1Click(Sender: TObject);
    procedure TimerRotateMultiPlanTimer(Sender: TObject);
    procedure TimerPowerMonitorTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerMediaRotatorTimer(Sender: TObject);
  private

    fAutoRestartCommand : String;

    fVisibleCursor,
    fExternalMediaPlayer,
    fLogRetryOn32,
    fRestartOnNewPlans,
    fRestartOnNewPgms,
    fRestartOnException : Boolean;
    fDisplayZipProgress,
    fBackup,
    fAlwaysOnTop,
    fSleepAvailable,
    fApplicationLog,
    fLogWindow,
    fProtectFromDownload,
    fTimersActivated,
    fAlignToGrid,
    fLockLoading,
    fRebootOnException:Boolean;

    lLastPowerState : Boolean;
    nPowerOnSequence,
    nPowerOffSequence,
    nMaxMonitorToAddress,
    nGridSpace : Integer;
    sNewPlansFolder,
    sExtractedPlanFolder,
    sArchivesFolder,
    sLogFileName : String;
    fRotate:integer;
    fVideoPlayer_CommandLine : String;
    fDisplayMonitor: boolean;

    procedure SetAlignToGrid(_Value : Boolean);
    procedure SetVisibleCursor(const Value: boolean);
    procedure SetRotate(Value : Integer);
    procedure SetDisplayZipProgress(const Value: Boolean);
    function PlanHasToBeForced: boolean;
    procedure StopAllPlayers;
    procedure SetProtectFromDownload(const Value: Boolean);
    function NoDownloadFileName: String;
    procedure SetTimersActivated(const Value: boolean);

    function PlansFileName: String;
    function ItemsFileName: String;
    function PosFileName: String;
    function MonitorsFileName: String;
    function ConfigFileName : String;

    procedure LogWindow(_String: String);
    procedure SetAlwaysOnTop(const Value: Boolean);
    procedure DetectAndActivateNewPlans(_FirstTime : Boolean = False);

    function CheckRecentFile(_FileName: String; _LastDate: TDateTime): Boolean;
    procedure SetArchiveToKo(_ArchiveFileName: String);
    function ConvertRebootTime(_RebootTime : String) : TDateTime;
    procedure SetRebootTime(_RebootTime : TDateTime);
    procedure MyException(Sender: TObject; E: Exception);

  public
    LastPlanDateTime:TDateTime;
    LastConfigDateTime:TDateTime;
    LastObjectsDateTime:TDateTime;
    LastPosDateTime:TDateTime;
    LastPlan:string;
    SubPLan:string;
    IsMoving:boolean;
    Speed:integer;
    MonitorPowerTimeOff:string;
    MonitorPowerTimeOn:string;
    TabMedias:TStringlist;
    procedure ReadParameters();
    procedure PostCreate(var msg:TMessage);message WM_USER;
    procedure PostCancel(var msg:TMessage);message WM_USER+1;
    function ForcePlanFileName : String;
    function CheckAndLoadCurrentPlan() : Boolean;
    function IsVeilleOn : Boolean;
    procedure CheckMonitorPower();
    procedure SwitchMonitorPower(_On:boolean);
    procedure LoadPgms(_ForcePlan : Boolean = False);
    procedure LoadPlans();
    procedure SavePlans();
    function CurrentPlan():string;
    procedure DisplayEditMode(b: boolean);

    property DisplayMonitor : boolean read fDisplayMonitor write fDisplayMonitor;
    property VisibleCursor : boolean read fVisibleCursor write SetVisibleCursor;
    property ExternalMediaPlayer : boolean read fExternalMediaPlayer write fExternalMediaPlayer;
    property LogRetryOn32 : boolean read fLogRetryOn32 write fLogRetryOn32;
    property RestartOnNewPlans : boolean read fRestartOnNewPlans write fRestartOnNewPlans;
    property RestartOnNewPgms : boolean read fRestartOnNewPgms write fRestartOnNewPgms;

    property Rotate : Integer read fRotate write SetRotate;
    property DisplayZipProgress : Boolean read fDisplayZipProgress write SetDisplayZipProgress;
    property Backup : Boolean read fBackup write fBackup;
    property ProtectFromDownload : Boolean read fProtectFromDownload write SetProtectFromDownload;
    property TimersActivated : boolean read fTimersActivated write SetTimersActivated;
    property AlignToGrid : boolean read fAlignToGrid write SetAlignToGrid;
    property AlwaysOnTop : Boolean read fAlwaysOnTop write SetAlwaysOnTop;
    property VideoPlayer_CommandLine : String read fVideoPlayer_CommandLine write fVideoPlayer_CommandLine;
    property LockLoading : Boolean read fLockLoading write fLockLoading;

    procedure hBoardLog(_String : String; _Exception : Boolean = False); overload;
    procedure hBoardLog(_Exception : Exception; _ComplementInfo : String = ''); overload;

    procedure CloseApplication;
    procedure RestartApplication;

    function ForceGridPosition(_Position: integer): integer;

  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  Inifiles, wndDisplay, uCSV2DBfunctions, UNew, wndProperties, DateUtils,
  unitblack, uCommon, wndZipAndUnzip, wndLogWindow, uBoardLiteCommon,
  ShellApi, uFileInUse, STrUtils;

procedure TfrmMain.CoolTrayIcon1Click(Sender: TObject);
begin
  Application.BringToFront();
end;

function TfrmMain.CheckRecentFile(_FileName : String; _LastDate : TDateTime) : Boolean;
begin
  Result := False;
  try
    Result := not VisibleCursor and (FileDateToDateTime(FileAge(_FileName))>_LastDate);
  except
    On E:Exception do hBoardLog(E);
  end;
end;

procedure TfrmMain.LoadPgms(_ForcePlan : Boolean = False);

  procedure ResetAllMediaForms();
  var
    nCount,
    nCurrentDisplayWindows,
    nMaxWindows : Integer;
    frmDisplay : TfrmDisplay;
  begin
    nCurrentDisplayWindows := 0;
    nMaxWindows := Iif(nMaxMonitorToAddress=-1, Screen.MonitorCount, nMaxMonitorToAddress);
    for nCount := Screen.FormCount-1 downto 0 do
      if Screen.Forms[nCount] is TfrmDisplay then nCurrentDisplayWindows := nCurrentDisplayWindows + 1;

    if not (nCurrentDisplayWindows = nMaxWindows) then
      for nCount := Screen.FormCount-1 downto 0 do
        if Screen.Forms[nCount] is TfrmDisplay then
          Screen.Forms[nCount].Close();
      for nCount:=0 to nMaxWindows-1 do
      begin
        hBoardLog('Create Screen ' + IntToStr(nCount));
          frmDisplay := TfrmDisplay.Create(Self);
        hBoardLog('Affect Screen Id');
        frmDisplay.ScreenId:=nCount+1;
        if fAlwaysOnTop then frmDisplay.FormStyle := fsStayOnTop;
      end;
    for nCount := 0 to nMaxWindows-1 do
      if Screen.Forms[nCount] is TfrmDisplay then
      begin
        hBoardLog('PlayMedia');
        TfrmDisplay(Screen.Forms[nCount]).PlayNextMedia();
        TfrmDisplay(Screen.Forms[nCount]).GoDisplay(True);
      end;
  end;

begin
  if not LockLoading then
  begin
    LockLoading := True;
    try LastObjectsDateTime:=FileDateToDateTime(FileAge(ItemsFileName)); except On E:Exception do hBoardLog(E); end;
    try LastPosDateTime:=FileDateToDateTime(FileAge(PosFileName));      except On E:Exception do hBoardLog(E); end;
    ResetAllMediaForms();
    IsMoving:=false;
    LockLoading := False;
  end;
end;

procedure TfrmMain.ReadParameters();
var
  nCount : Integer;
  f:TextFile;
  ini:TIniFile;
  lstRebootTime : TStringList;

begin

  hBoardLog('Read new Parameters');

  if not FileExists(ConfigFileName) then
  begin
    AssignFile(f, ConfigFileName);
    Rewrite(f);
    WriteLn(f, '[config]');
    CloseFile(f);
  end;

  ini:=TIniFile.Create(ConfigFileName);
  Rotate:=ini.ReadInteger(CST_CONFIG_SECTION,CST_ROTATE_DELAY,10);
  ini.WriteInteger(CST_CONFIG_SECTION,CST_ROTATE_DELAY,Rotate);

  nGridSpace:=ini.ReadInteger(CST_CONFIG_SECTION,'grid',1);
  ini.WriteInteger(CST_CONFIG_SECTION,'grid',nGridSpace);

  AlignToGrid:=ini.ReadBool(CST_CONFIG_SECTION,'align',True);

  Speed:=ini.ReadInteger(CST_CONFIG_SECTION,'Speed',10);
  ini.WriteInteger(CST_CONFIG_SECTION,'speed',Speed);

  MonitorPowerTimeOff:=ini.ReadString(CST_CONFIG_SECTION,'time_screen_off','');
  ini.WriteString(CST_CONFIG_SECTION,'time_screen_off',MonitorPowerTimeOff);

  MonitorPowerTimeOn:=ini.ReadString(CST_CONFIG_SECTION,'time_screen_on','');
  ini.WriteString(CST_CONFIG_SECTION,'time_screen_on',MonitorPowerTimeOn);

  DisplayZipProgress:=ini.ReadBool(CST_CONFIG_SECTION,CST_DISPLAY_ZIP_PROGRESS,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_DISPLAY_ZIP_PROGRESS,DisplayZipProgress);

  VisibleCursor:=ini.ReadBool(CST_CONFIG_SECTION,CST_VISIBLE_CURSOR,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_VISIBLE_CURSOR,VisibleCursor);

  DisplayMonitor:=ini.ReadBool(CST_CONFIG_SECTION,CST_DISPLAY_MONITOR,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_DISPLAY_MONITOR,DisplayMonitor);

  ExternalMediaPlayer:=ini.ReadBool(CST_CONFIG_SECTION,CST_EXT_MEDIA_PLAYER,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_EXT_MEDIA_PLAYER,ExternalMediaPlayer);

  LogRetryOn32:=ini.ReadBool(CST_CONFIG_SECTION,CST_LOG_RETRY_ON_32,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_LOG_RETRY_ON_32,LogRetryOn32);

  RestartOnNewPlans:=ini.ReadBool(CST_CONFIG_SECTION,CST_RESTART_ON_NEW_PLANS,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_RESTART_ON_NEW_PLANS,RestartOnNewPlans);

  RestartOnNewPgms:=ini.ReadBool(CST_CONFIG_SECTION,CST_RESTART_ON_NEW_PGMS,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_RESTART_ON_NEW_PGMS,RestartOnNewPgms);

  Backup:=ini.ReadBool(CST_CONFIG_SECTION,CST_BACKUP,True);

  nMaxMonitorToAddress:=ini.ReadInteger(CST_CONFIG_SECTION,'MaxMonitorToAddress',-1);
  ini.WriteInteger(CST_CONFIG_SECTION,'MaxMonitorToAddress',nMaxMonitorToAddress);

  nPowerOffSequence:=ini.ReadInteger(CST_CONFIG_SECTION,'power_off_sequence',2);
  if nPowerOffSequence = -1 then nPowerOffSequence := 2;
  ini.WriteInteger(CST_CONFIG_SECTION,'power_off_sequence',nPowerOffSequence);

  nPowerOnSequence:=ini.ReadInteger(CST_CONFIG_SECTION,'power_on_sequence',-1);
  if nPowerOnSequence = -1 then nPowerOnSequence := -1;
  ini.WriteInteger(CST_CONFIG_SECTION,'power_on_sequence',nPowerOnSequence);

  AlwaysOnTop:=ini.ReadBool(CST_CONFIG_SECTION,'Always_On_Top',False);

  sLogFileName:=ini.ReadString(CST_CONFIG_SECTION,CST_LOG_FILENAME,'');
  fApplicationLog:=ini.ReadBool(CST_CONFIG_SECTION,CST_APPLICATION_LOG,False);
  fLogWindow:=ini.ReadBool(CST_CONFIG_SECTION,CST_LOG_WINDOW,False);
  fSleepAvailable:=ini.ReadBool(CST_CONFIG_SECTION,CST_SLEEP_AVAILABLE,True);

  sNewPlansFolder := ini.ReadString(CST_CONFIG_SECTION,'NewPlansFolder','');
  sExtractedPlanFolder := ini.ReadString(CST_CONFIG_SECTION,'ExtractedPlanFolder','');
  sArchivesFolder := ini.ReadString(CST_CONFIG_SECTION,'ArchivesFolder','');

  lstRebootTime := TStringList.Create();
  Split(';', ini.ReadString(CST_CONFIG_SECTION,'RebootTime',''), lstRebootTime);
  for nCount := 0 to lstRebootTime.Count-1 do
    lstRebootTime[nCount] := FloatToStr(ConvertRebootTime(lstRebootTime[nCount]));
  lstRebootTime.Sort;
  for nCount := 0 to lstRebootTime.Count-1 do
    if StrToFloat(lstRebootTime[nCount]) > now() then
    begin
      SetRebootTime(StrToFloat(lstRebootTime[nCount]));
      break;
    end;
  lstRebootTime.Free();

  fAutoRestartCommand := ini.ReadString(CST_CONFIG_SECTION,'AutoRestart','');
  if not (fAutoRestartCommand = '') then
  begin
    if not FileExists(fAutoRestartCommand) then fAutoRestartCommand := AddSlash(ExtractFilePath(Application.ExeName)) + fAutoRestartCommand;
    if FileExists(fAutoRestartCommand) then Application.OnException := MyException;
  end;

  fRebootOnException := ini.ReadBool(CST_CONFIG_SECTION,CST_REBOOT_ON_EXCEPTION,False);
  ini.WriteBool(CST_CONFIG_SECTION,CST_REBOOT_ON_EXCEPTION,fRebootOnException);

  fRestartOnException:=ini.ReadBool(CST_CONFIG_SECTION,CST_RESTART_ON_EXCEPTION,True);
  ini.WriteBool(CST_CONFIG_SECTION,CST_RESTART_ON_EXCEPTION,fRestartOnException);

  fVideoPlayer_CommandLine := ini.ReadString(CST_CONFIG_SECTION,'VideoPlayer_CommandLine','');
  if Trim(fVideoPlayer_CommandLine) = '' then hBoardLog('No Video parameter. Won''t play any video');
  ini.UpdateFile();
  ini.Free();

  if FileExists(ConfigFileName) then
  begin
    AssignFile(f, ConfigFileName);
    Reset(F);
    CloseFile(f);
  end;

  try LastConfigDateTime:=FileDateToDateTime(FileAge(ConfigFileName)); except On E:Exception do hBoardLog(E); end;

end;

procedure TfrmMain.MyException(Sender: TObject; E: Exception);
begin
  hBoardLog(E, 'Uncatched Exception');
  if fRestartOnException then RestartApplication();
end;

procedure TfrmMain.CloseApplication;
begin
  hBoardLog('Closing...');
  Application.Terminate;
end;

procedure TfrmMain.RestartApplication();
begin
  TimersActivated := False;
  hBoardLog('Restarting...');
  WinExec(PAnsiChar(fAutoRestartCommand), SW_SHOWMINIMIZED);
  CloseApplication;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  f:TextFile;
  i:integer;
begin

  lLastPowerState := True;
  IsMoving:=false;
  VisibleCursor:=false;

  if not FileExists('c:\windows\system32\dplayocx.dll') then CloseApplication;

  if not FileExists(MonitorsFileName) then
  begin
    AssignFile(f, MonitorsFileName);
    Rewrite(f);
    WriteLn(f, ExtractFilePath(Application.Exename)+'monitor;media');
    for i:=1 to 8 (*Screen.MonitorCount*) do
    begin
      WriteLn(f, IntToStr(i)+';empty.bmp;');
    end;
    CloseFile(f);
  end;
  if not FileExists(ItemsFileName) then
  begin
    AssignFile(f, ItemsFileName);
    Rewrite(f);
    WriteLn(f, 'Id;Label;Screen;Width;Height;Left;Top;Font;Size;Bold;Color;Mobile;Reserved;');
    CloseFile(f);
  end;
  if not FileExists(PosFileName) then
  begin
    AssignFile(f, PosFileName);
    Rewrite(f);
    WriteLn(f, 'Code;Short Description;Long Description;Price1;Price2;Price3;Price4;Price5;Stock;');
    CloseFile(f);
  end;
  LastConfigDateTime:=0;
  LastPlanDateTime:=0;
  LastObjectsDateTime:=0;
  ReadParameters();

  TabMedias:=TStringList.Create();
  LoadPlans();
  LastPlan:=''; // CurrentPlan();
  SubPlan:='';

  // Waiting for the log file to be closed...
  Sleep(50);

  PostMessage(Handle,WM_USER,0,0);

  TimersActivated := True;

end;

procedure TfrmMain.PostCreate(var msg: TMessage);
begin
  FormBlack:=TFormBlack.Create(Self);
  FormBlack.color:=clBlack;
  FormBlack.BorderStyle:=bsNone;
  FormBlack.BoundsRect:=Screen.DesktopRect;
  FormBlack.Show();
  Self.Hide();
  DetectAndActivateNewPlans(True);
  FormBlack.Close();
end;

procedure TfrmMain.StopAllPlayers;
var
  nCountScreens : Integer;
begin
  for nCountScreens := Screen.FormCount-1 downto 0 do
    if Screen.Forms[nCountScreens] is TfrmDisplay then
      TfrmDisplay(screen.Forms[nCountScreens]).StopVideo();
end;

procedure TfrmMain.SetArchiveToKo(_ArchiveFileName : String);
begin
  RenameFile(_ArchiveFileName, AddSlash(ExtractFileDir(_ArchiveFileName)) + ExtractOnlyFileName(_ArchiveFileName) + CST_ARCHIVE_KO_EXT)
end;

procedure TfrmMain.DetectAndActivateNewPlans(_FirstTime : Boolean = False);
var
  lIsNewPgms : Boolean;

  function IsSystemFolder(_Path : String) : Boolean;
  begin
    Result := (_Path = sNewPlansFolder) or (_Path = sExtractedPlanFolder) or (_Path = sArchivesFolder);
  end;

  procedure ArchiveLocalData();
  var
    frmZipAndUnzip : TfrmZipAndUnzip;
    nCountItems : Integer;
    ItemsToSave,
    FoldersToSave : TStringList;
    fArchiveName : String;
  begin

    if not DirectoryExists(sArchivesFolder) then CreateDir(sArchivesFolder);
    if DirectoryExists(sArchivesFolder) then
    begin
      ItemsToSave := TStringList.Create();
      FoldersToSave := TStringList.Create();
      GetAllFiles(ExtractFilePath(Application.ExeName), '*.*', ItemsToSave);
      GetAllFolders(ExtractFilePath(Application.ExeName), FoldersToSave);

      for nCountItems := 0 to FoldersToSave.Count-1 do
        if not IsSystemFolder(FoldersToSave[nCountItems]) then ItemsToSave.Add(FoldersToSave[nCountItems]);
      for nCountItems := ItemsToSave.Count-1 downto 0 do
        if ('.'+LowerCase(ExtractOnlyFileExt(ItemsToSave[nCountItems])) = LowerCase(CST_ARCHIVE_EXT)) or
           ('.'+LowerCase(ExtractOnlyFileExt(ItemsToSave[nCountItems])) = LowerCase(CST_LOG_EXT)) then
          ItemsToSave.Delete(nCountItems);

      fArchiveName := AddSlash(sArchivesFolder) + GetArchiveFileName + CST_ARCHIVE_EXT;
      hBoardLog('Archived Creation : ' + fArchiveName);

      frmZipAndUnzip := TfrmZipAndUnzip.Create(Self);
      frmZipAndUnzip.CreateArchive(
          ItemsToSave,
          ExtractFilePath(Application.ExeName),
          ExtractFilePath(Application.ExeName),
          fArchiveName,
          'Sauvegarde des travaux en cours...',
          DisplayZipProgress,
          True);
      frmZipAndUnzip.Free();

      ItemsToSave.Free();
      FoldersToSave.Free();
      hBoardLog('Archived Created');
    end
    else
      Showmessage('Impossible de cr�er le r�pertoire de sauvegarde ' + sArchivesFolder + ', sauvegarde impossible');
  end;

  function IsThereNewPlan(_ArchivesFiles : TStringList; _Today : String; var _NewPlanName : String) : boolean;
  begin
    result := (_ArchivesFiles.Count > 0) and (ExtractOnlyFileName(_ArchivesFiles[0]) <= _Today) and not (GetFileStatus(AddSlash(sNewPlansFolder)+_ArchivesFiles[0]) = Lock);
    if Result then
      _NewPlanName := AddSlash(sNewPlansFolder)+_ArchivesFiles[0]
    else
      _NewPlanName := '';
  end;

  function ExtractNextPlans(_ArchivesFiles : TStringList; _Today : String; var _ArchiveFileName : String) : boolean;
  var
    frmZipAndUnzip : TfrmZipAndUnzip;

  begin
    Result := False;
    if not DirectoryExists(sExtractedPlanFolder) then
      CreateDir(sExtractedPlanFolder)
    else
      DeleteDirectory(sExtractedPlanFolder);
    if DirectoryExists(sExtractedPlanFolder) and FileExists(_ArchiveFileName) then
    try
      hBoardLog('Processing archive : ' + _ArchiveFileName + ' ('+BoolToStr(FileExists(_ArchiveFileName))+') in ' + sExtractedPlanFolder + '('+BoolToStr(DirectoryExists(sExtractedPlanFolder))+')...');
      frmZipAndUnzip := TfrmZipAndUnzip.Create(Self);
      Result := frmZipAndUnzip.LoadArchive( _ArchiveFileName,
                                            sExtractedPlanFolder,
                                            'D�compression de l''archive ' + _ArchiveFileName + ' en cours...',
                                            DisplayZipProgress,
                                            False);
      frmZipAndUnzip.Free();
    except
      On E:Exception do hBoardLog(E);
    end;
    if not Result then
    begin
      SetArchiveToKo(_ArchiveFileName);
      hBoardLog('A problem occured with archive : ' + _ArchiveFileName);
      if not DirectoryExists(sExtractedPlanFolder) then hBoardLog(' - '+ sExtractedPlanFolder + ' does not exist');
      if not FileExists(_ArchiveFileName) then hBoardLog(' - '+ _ArchiveFileName + ' does not exist');
    end;
  end;

  function InstallNewPlans() : Boolean;
  var
    nRet : Integer;
    SearchRec : TSearchRec;
  begin
    Result := False;
    Try
      hBoardLog('New plans installation');
      nRet := FindFirst('*.*', faDirectory, SearchRec);
      While nRet = 0 do
      begin
        if (faDirectory and SearchRec.Attr = faDirectory) and
            not IsSystemFolder(SearchRec.Name) and
            not (SearchRec.Name[1] = '.') and
            not (SearchRec.Name = ExtractDeepestDir(AddSlash(sNewPlansFolder))) and
            not (SearchRec.Name = ExtractDeepestDir(AddSlash(sArchivesFolder))) and
            not (SearchRec.Name = ExtractDeepestDir(AddSlash(sExtractedPlanFolder))) then
          DeleteDirectory(SearchRec.Name, True);
        nRet := FindNext(SearchRec);
      end;
      FindClose(SearchRec);

      nRet := FindFirst(AddSlash(sExtractedPlanFolder) + '*.*', faAnyFile, SearchRec);
      While nRet = 0 do
      begin
        if not (SearchRec.Name[1] = '.') and not (SearchRec.Name = CST_CONFIG_INI_FILENAME) then
          if (faDirectory and SearchRec.Attr = faDirectory) then
            MoveDir(AddSlash(sExtractedPlanFolder) + SearchRec.Name, ExtractFileDir(Application.ExeName))
          else
          begin
            MoveFiles(AddSlash(sExtractedPlanFolder) + SearchRec.Name, AddSlash(ExtractFileDir(Application.ExeName)), False);
            FileSetDate(AddSlash(ExtractFileDir(Application.ExeName)) + SearchRec.Name, DateTimeToFileDate(Now()));
          end;
        nRet := FindNext(SearchRec);
      end;
      FindClose(SearchRec);
      Result := True;
    Except
      On E:Exception do hBoardLog(E);
    End;
  end;

  procedure GetNewParameters();
  var
    iniFile : TIniFile;
  begin
    iniFile := TiniFile.Create(AddSlash(sExtractedPlanFolder) + CST_NEW_PARAMETERS_FILENAME);
    Rotate := iniFile.ReadInteger(CST_CONFIG_SECTION, CST_ROTATE_DELAY, 10);
    iniFile.Free();
    iniFile := TIniFile.Create(ConfigFileName);
    iniFile.WriteInteger(CST_CONFIG_SECTION,CST_ROTATE_DELAY,Rotate);
    iniFile.Free();
    ReadParameters();
  end;

  function CheckAndInstallNewPlans() : Boolean;
  var
    nCount : Integer;
    NewFiles : TStringList;
    sbackupFileName,
    sArchiveFileName,
    sToday : String;
//    dStartTime : TDateTime;
    lAbort : Boolean;
  begin
    Result := False;
    sToday := FormatDateTime('yyyymmdd', Now());
    sArchiveFileName := '';
    NewFiles := TStringList.Create();
    NewFiles.Sorted := True;
    if DirectoryExists(sNewPlansFolder) then
    begin
      GetAllFiles(sNewPlansFolder, '*.zip', NewFiles);
      NewFiles.Sort;
      if (NewFiles.Count > 0) and (IsThereNewPlan(NewFiles, sToday, sArchiveFileName)) then
      begin
        hBoardLog('New plans detected : ' + sArchiveFileName);
        lAbort := False;
{*        dStartTime := Now;
        if not (GetProcessIdUsingFile(sArchiveFileName) = 0) then
        begin
          hBoardLog(sArchiveFileName + ' locked. Waiting 2'' for it to be released.');
          while (not (GetProcessIdUsingFile(sArchiveFileName) = 0)) and
  //              (not (GetProcessIdUsingFile(sArchiveFileName) = Application.Handle)) and
                (not lABort) do
            lAbort := Now > dStartTime + EncodeTime(00, 02, 00, 00);
        end;                                                        *}
        if not lAbort then
        begin
          StopAllPlayers();
          TimersActivated := False;
          if ExtractNextPlans(NewFiles, sToday, sArchiveFileName) then
          try
            if not (DirectoryExists(sArchivesFolder)) then CreateDir(sArchivesFolder);
            ProtectFromDownload := True;
            // Attention en cas de Failed Successifs !!!
            // A G�rer
            if Backup then ArchiveLocalData();
            if InstallNewPlans() then
            begin
              GetNewParameters();
              DeleteDirectory(sExtractedPlanFolder);
              sbackupFileName := AddSlash(sNewPlansFolder)+ExtractOnlyFileName(sArchiveFileName);
              if FileExists(sbackupFileName + '.sav') then
              begin
                nCount := 1;
                while FileExists(sbackupFileName + '.' + IntToStr(nCount) + '.sav') do Inc(nCount);
                RenameFile(sArchiveFileName, sbackupFileName + '.' + IntToStr(nCount) + '.sav');
              end
              else
                RenameFile(sArchiveFileName, sbackupFileName + '.sav');
              CreateEmptyFile(ForcePlanFileName());
            end
            else
              SetArchiveToKo(sArchiveFileName);
            ProtectFromDownload := False;
            Result := True;
          except
            On E:Exception do hBoardLog(E, 'All timers could be desactived, please check configuration and restart application.');
          end;
        end
        else
          hBoardLog(sArchiveFileName + ' locked for too long time, aborted.');
      end;
    end;
    NewFiles.Free();
  end;
begin
  if fTimersActivated then
  begin
    if CheckRecentFile(ConfigFileName, LastConfigDateTime) then ReadParameters();
    lIsNewPgms := CheckAndInstallNewPlans();
    if lIsNewPgms or (CheckRecentFile(PlansFileName, LastPlanDateTime)) then
      if RestartOnNewPlans then
        RestartApplication
      else
        LoadPlans();
    lIsNewPgms := CheckAndLoadCurrentPlan() or lIsNewPgms;
    lIsNewPgms := CheckRecentFile(ItemsFileName, LastObjectsDateTime) or
                  CheckRecentFile(PosFileName  , LastPosDateTime) or
                 lIsNewPgms;
    if lIsNewPgms then
      if RestartOnNewPgms and not _FirstTime then
        RestartApplication
      else
      begin
        TimersActivated := False;
        LoadPgms();
        TimersActivated := True;
      end;
  end;
end;

procedure TfrmMain.TimerCheckNewPgmsTimer(Sender: TObject);
begin
  DetectAndActivateNewPlans();
end;

procedure TfrmMain.Quitter1Click(Sender: TObject);
begin
  Close();
end;

procedure TfrmMain.SetVisibleCursor(const Value: boolean);
begin
  fVisibleCursor := Value;
  ShowCursor(fVisibleCursor);
end;

procedure TfrmMain.PostCancel(var msg: TMessage);
begin
  IsMoving:=false;
end;

function TfrmMain.PlansFileName : String;
begin
  Result := ExtractFilePath(Application.Exename)+'plan.csv';
end;

function TfrmMain.ItemsFileName : String;
begin
  Result := ExtractFilePath(Application.Exename)+'item.csv';
end;

function TfrmMain.PosFileName : String;
begin
  Result := ExtractFilePath(Application.Exename)+'pos.csv';
end;

function TfrmMain.MonitorsFileName : String;
begin
  Result := ExtractFilePath(Application.Exename)+'monitor.csv';
end;

function TfrmMain.ConfigFileName : String;
begin
  Result := ExtractFilePath(Application.Exename)+'hboardlite.ini';
end;

procedure TfrmMain.LoadPlans;
var
  data:TStringList;
  i:integer;
  s:string;
begin
  try
    if FileExists(PlansFileName) then LastPlanDateTime:=FileDateToDateTime(FileAge(PlansFileName));
  except
    On E:Exception do hBoardLog(E);
  end;
  cdsDay.Close();
  cdsDay.CreateDataSet();
  cdsDay.Append(); cdsDayday.Value:=1; cdsDaylib.Value:='Monday'; cdsDay.Post();
  cdsDay.Append(); cdsDayday.Value:=2; cdsDaylib.Value:='Tuesday'; cdsDay.Post();
  cdsDay.Append(); cdsDayday.Value:=3; cdsDaylib.Value:='Wednesday'; cdsDay.Post();
  cdsDay.Append(); cdsDayday.Value:=4; cdsDaylib.Value:='Thursday'; cdsDay.Post();
  cdsDay.Append(); cdsDayday.Value:=5; cdsDaylib.Value:='Friday'; cdsDay.Post();
  cdsDay.Append(); cdsDayday.Value:=6; cdsDaylib.Value:='Saturday'; cdsDay.Post();
  cdsDay.Append(); cdsDayday.Value:=7; cdsDaylib.Value:='Sunday'; cdsDay.Post();

  cdsPlan.Close();
  cdsPlan.IndexDefs.Clear();
  with cdsPlan.IndexDefs.AddIndexDef do
  begin
    Name :='plan_dayhour';
    Fields := 'day;hour';
    Options := [];
  end;
  cdsPlan.IndexName:='plan_dayhour';

  cdsPlan.CreateDataSet();

  if FileExists(PlansFileName) then
  begin
    hBoardLog('Plan loading : ' + PlansFileName);
    data:=TStringList.Create();
    try
      data.Text:=LoadCSVinStrings(PlansFileName);
      for i:=1 to data.count-1 do
      begin
        s:=data[i];
        cdsPlan.Append();
        cdsPlanday.Value:=(GetCSVcell(s,0));
        cdsPlanhour.Value:=(GetCSVcell(s,1));
        cdsPlanfolder.Value:=(GetCSVcell(s,2));
        cdsPlan.Post();
      end;
    except
      On E:Exception do hBoardLog(E);
    end;
    data.Free();
  end;
end;

procedure TfrmMain.SavePlans;
var
  s:string;
  data:TStringList;
begin
  data:=TStringList.Create();
  data.add('day;hour;folder;');
  try
    cdsPlan.Post();
  except
    On E:Exception do hBoardLog(E);
  end;
  cdsPLan.First();
  while not cdsPLan.Eof do
  begin
    s:=';;;';
    s:=SetCSVcell(s,0,(cdsPlanday.value));
    s:=SetCSVcell(s,1,(cdsPlanhour.value));
    s:=SetCSVcell(s,2,(cdsPlanfolder.value));
    data.add(s);
    cdsPLan.Next();
  end;
  SaveStringsToCSV(PlansFileName, data.Text);
  data.Free();
end;

function TfrmMain.CurrentPlan: string;
var
  now1:TDateTime;
  day1,
  hour1:string;
begin
  now1:=now();
  day1:=IntToStr(DayOfTheWeek(now1));
  hour1:=Copy(TimeToStr(now1), 1, 8);

  cdsPlan.First();

  while (day1+'-'+hour1 >= cdsPlanDay.Value+'-'+cdsPlanhour.Value)
    and (not cdsPlan.Eof) do
  begin
    cdsPlan.Next();
  end;
  cdsPlan.Prior();

  result:=cdsPlanfolder.value;

end;

function TfrmMain.ForcePlanFileName : String;
begin
  Result := AddSlash(ExtractFilePath(Application.ExeName))+CST_FORCEPLAN_FILENAME;
end;

function TfrmMain.PlanHasToBeForced() : boolean;
begin
  result := FileExists(ForcePlanFileName());
  if result then DeleteFile(ForcePlanFileName())
end;

Function TfrmMain.CheckAndLoadCurrentPlan : Boolean;
var
  NewPlan:string;
  MultiPlans:TStringList;
  lForcePlan : Boolean;

begin
  Result := False;
  NewPlan:=CurrentPlan();
  lForcePlan := PlanHasToBeForced();
  if (NewPlan<>LastPlan) or lForcePlan then
  begin
    hBoardLog('Plan installing : ' + NewPlan);
    LastPlan:=NewPlan;
    if pos(',', NewPlan)>0 then // plan_a,plan_b,5
    begin
      MultiPlans:=TStringList.Create();
      MultiPlans.text:=StringReplace(NewPlan,',',#13#10,[rfReplaceAll]);
      SubPlan:=MultiPlans[0];
      TimerRotateMultiPlan.Interval:= StrToInt(MultiPlans[MultiPlans.Count-1]) * 1000;
    end
    else
      SubPlan:=NewPlan;
    CopyFile(PAnsiChar(SubPlan+'\monitor.csv'), '.\monitor.csv', false);
    CopyFile(PAnsiChar(SubPlan+'\item.csv'), '.\item.csv', false);
    Result := True;
  end;
end;

procedure TfrmMain.TimerRotateMultiPlanTimer(Sender: TObject);
var
  MultiPlans:TStringList;
begin
  if (LastPlan<>SubPlan) and TimersActivated then // MultiPlan active
  begin
    MultiPlans:=TStringList.Create();
    MultiPlans.text:=StringReplace(LastPlan,',',#13#10,[rfReplaceAll]);
    if MultiPlans.IndexOf(SubPlan)=-1 then // Old SubPlan not found
    begin
      SubPlan:=MultiPlans[0];
    end
    else
    begin
      if MultiPlans.IndexOf(SubPlan)=MultiPlans.Count-2 then // Last Subplan reached
      begin
        SubPlan:=MultiPlans[0];
      end
      else
      begin
        SubPlan:=MultiPlans[MultiPlans.IndexOf(SubPlan)+1];
      end;
    end;
    MultiPlans.Free();
    CopyFile(PAnsiChar(SubPlan+'\monitor.csv'), '.\monitor.csv', false);
    CopyFile(PAnsiChar(SubPlan+'\item.csv'), '.\item.csv', false);
    LoadPgms();
  end;
end;

procedure TfrmMain.DisplayEditMode(b: boolean);
var
  i:integer;
begin
  for i:=Screen.FormCount-1 downto 0 do
  begin
    if Screen.Forms[i] is TfrmDisplay then
    begin
      try
        (Screen.Forms[i] as TfrmDisplay).LabelEditMode.Visible:=b;
      except
        On E:Exception do hBoardLog(E);
      end;
      try
        (Screen.Forms[i] as TfrmDisplay).TimerMediaRotator.Enabled:=not(b);
      except
        On E:Exception do hBoardLog(E);
      end;
    end;
  end;
end;

function TfrmMain.IsVeilleOn : Boolean;
begin
  Result := (CurrentPlan = CST_SLEEP_PLAN);
end;

procedure TfrmMain.CheckMonitorPower;
var
  tow:string;
begin
  tow:=Copy(TimeToStr(Now()), 1, 5);
  if fSleepAvailable then
  begin
    if (tow=MonitorPowerTimeOff) or IsVeilleOn then SwitchMonitorPower(false);
    if (tow=MonitorPowerTimeOn) or not IsVeilleOn then SwitchMonitorPower(true);
  end;
end;

procedure TfrmMain.SwitchMonitorPower(_On: boolean);
begin
  if _On <> lLastPowerState then
  begin
    if _On then
      PostMessage(Application.Handle, WM_SYSCOMMAND, SC_MONITORPOWER, nPowerOnSequence)
    else
      PostMessage(Application.Handle, WM_SYSCOMMAND, SC_MONITORPOWER, nPowerOffSequence);
    hBoardLog('Sleep ' + Iif(_On, 'Off', 'On'));
    if fApplicationLog then
      if _On then
        Windows.Beep(440, 125)
      else
        Windows.Beep(110, 125);

  end;
  lLastPowerState := _On;
end;

procedure TfrmMain.TimerPowerMonitorTimer(Sender: TObject);
begin
  if TimersActivated then CheckMonitorPower();
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  nCountScreens : Integer;
begin
  TimersActivated := False;
  for nCountScreens := Screen.FormCount-1 downto 0 do
    if Screen.Forms[nCountScreens] is TfrmDisplay then
      TfrmDisplay(screen.Forms[nCountScreens]).Close;
  TabMedias.Free();
end;

procedure TfrmMain.SetRotate(Value: Integer);
begin
  fRotate := Value;
  TimerMediaRotator.Interval:=1000*fRotate;
end;

procedure TfrmMain.TimerMediaRotatorTimer(Sender: TObject);
var
  nCountScreens : Integer;
begin
  if TimersActivated then
  begin
    hBoardLog('Start Media Rotate');
    TimerMediaRotator.Enabled := False;
    for nCountScreens := 0 to Screen.FormCount-1 do
      if Screen.Forms[nCountScreens] is TfrmDisplay then
        TfrmDisplay(screen.Forms[nCountScreens]).PlayNextMedia();
    for nCountScreens := 0 to Screen.FormCount-1 do
      if Screen.Forms[nCountScreens] is TfrmDisplay then
        TfrmDisplay(screen.Forms[nCountScreens]).GoDisplay();
    TimerMediaRotator.Enabled := True;
    hBoardLog('End Media Rotate');
  end;
end;

procedure TfrmMain.SetDisplayZipProgress(const Value: Boolean);
begin
  fDisplayZipProgress := Value;
end;

function TfrmMain.NoDownloadFileName() : String;
begin
  result := AddSlash(sArchivesFolder) + CST_NO_DOWNLOAD_FILENAME;
end;

procedure TfrmMain.SetProtectFromDownload(const Value: Boolean);
begin
  fProtectFromDownload := Value;
  if fProtectFromDownload then
     CreateEmptyFile(NoDownloadFileName)
  else
    if FileExists(NoDownloadFileName) then DeleteFile(NoDownloadFileName);
end;

procedure TfrmMain.SetTimersActivated(const Value: boolean);
begin
  fTimersActivated := Value;
  TimerCheckNewPgms.Enabled := fTimersActivated;
  TimerRotateMultiPlan.Enabled := fTimersActivated;
  TimerPowerMonitor.Enabled := fTimersActivated;
  TimerMediaRotator.Enabled := fTimersActivated;
  if not fTimersActivated then StopAllPlayers;
end;

procedure TfrmMain.LogWindow(_String : String);
begin
  if fLogWindow then
  begin
    if frmLogWindow = nil then frmLogWindow := TfrmLogWindow.Create(Self);
    frmLogWindow.Display(_String);
    frmLogWindow.Show();
    frmLogWindow.BringToFront();
  end;
end;

procedure TfrmMain.SetAlignToGrid(_Value : Boolean);
var
  ini : TIniFile;
begin
  fAlignToGrid := _Value;
  ini:=TIniFile.Create(ConfigFileName);
  ini.WriteBool('config','align',fAlignToGrid);
  ini.Free();
end;

function TfrmMain.ForceGridPosition(_Position: integer): integer;
begin
  if AlignToGrid then
    Result := nGridSpace * ((_Position + nGridSpace div 2) div nGridSpace)
  else
    Result := _Position;
end;

procedure TfrmMain.hBoardLog(_String : String; _Exception : Boolean = False);
var
  sFilter : String;
begin
  sFilter := ''; //'(Screen : 2)';
  _String := FormatDateTime('YYYY/MM/DD-hh:nn:ss.zzz ', Now()) + Chr(9) + _String;
  if not (sLogFileName = '') and (_Exception or fApplicationLog) then Log(sLogFileName, _String + Iif(_Exception, ' (Exception)', ''), LogRetryOn32);
  if ((sFilter = '') or (RightStr(_String, Length(sFilter)) = sFilter)) then LogWindow(_String);
end;

procedure TfrmMain.hBoardLog(_Exception : Exception; _ComplementInfo : String = '');
begin
  hBoardLog(_Exception.ClassName + ' (' + DateTimeToStr(Now) + ') : ' + _Exception.Message + iif(not (Trim(_ComplementInfo)=''), ' (' +_ComplementInfo + ')', '') , True);
  if fRebootOnException then
    ShellExecute(0, nil, 'cmd.exe', PAnsiChar('/c Shutdown -r -f >> ' + sLogFileName), '', SW_HIDE);
end;

procedure TfrmMain.SetAlwaysOnTop(const Value: Boolean);
var
  ini : TIniFile;
begin
  fAlwaysOnTop := Value;
  ini:=TIniFile.Create(ConfigFileName);
  ini.WriteBool(CST_CONFIG_SECTION,'Always_On_Top',fAlwaysOnTop);
  ini.Free();
end;

function TfrmMain.ConvertRebootTime(_RebootTime : String) : TDateTime;
begin
  Result := 0;
  if not (_RebootTime = '') then
    try
      Result := StrToDateTimeFormat(FormatDateTime('dd/mm/yyyy ', Now()) + _RebootTime, 'dd/mm/yyyy', 'hh:nn');
      if Result < now() then Result := StrToDateTimeFormat(FormatDateTime('dd/mm/yyyy ', Now()+1) + _RebootTime, 'dd/mm/yyyy', 'hh:nn');
    except
      On E:Exception do hBoardLog(E, 'Could no properly set Reboot time (' + _RebootTime + ')' );
    end;
end;

procedure TfrmMain.SetRebootTime(_RebootTime : TDateTime);
begin
  if not (_RebootTime = 0) then
    try
      hBoardLog('Reboot time set to ' + FormatDateTime('dd/mm/yyyy hh:nn:ss', _RebootTime) + ' in ' + IntToStr(SecondsBetween(Now, _RebootTime)) + ' seconds');
      ShellExecute(0, nil, 'cmd.exe', PAnsiChar('/c Shutdown -r -f -t ' + IntToStr(SecondsBetween(Now, _RebootTime)) + ' >> ' + sLogFileName), '', SW_HIDE);
    except
    end
  else
    hBoardLog('No reboot time set');
end;

end.
