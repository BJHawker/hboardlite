unit uniteditpos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, DBClient, ExtCtrls, DBCtrls, StdCtrls;

type
  TFormEditPos = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    cdsPos: TClientDataSet;
    cdsPosShort: TStringField;
    cdsPosLong: TStringField;
    cdsPosCode: TStringField;
    cdsPosPrice1: TStringField;
    cdsPosPrice2: TStringField;
    cdsPosPrice3: TStringField;
    cdsPosPrice4: TStringField;
    cdsPosPrice5: TStringField;
    cdsPosStock: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { D�clarations priv�es }
  public
    procedure LoadPos();
    procedure SavePos();
  end;

var
  FormEditPos: TFormEditPos;

implementation

uses UNew;

{$R *.dfm}

procedure TFormEditPos.FormCreate(Sender: TObject);
begin
  LoadPos();
end;

procedure TFormEditPos.LoadPos;
var
  posCSV:TStringList;
  i:integer;
begin
  cdsPos.Close();
  cdsPos.CreateDataSet();
  posCSV:=TStringList.Create();
  posCSV.LoadFromFile(ExtractFilePath(Application.Exename)+'pos.csv');
  for i:=1 to posCSV.Count-1 do
  begin
    if GetCSVcell(posCSV[i],0)<>'' then
    begin
      cdsPos.Append();
      cdsPosCode.Value:=(GetCSVcell(posCSV[i],0));
      cdsPosShort.Value:=(GetCSVcell(posCSV[i],1));
      cdsPosLong.Value:=(GetCSVcell(posCSV[i],2));
      cdsPosPrice1.Value:=(GetCSVcell(posCSV[i],3));
      cdsPosPrice2.Value:=(GetCSVcell(posCSV[i],4));
      cdsPosPrice3.Value:=(GetCSVcell(posCSV[i],5));
      cdsPosPrice4.Value:=(GetCSVcell(posCSV[i],6));
      cdsPosPrice5.Value:=(GetCSVcell(posCSV[i],7));
      cdsPosStock.Value:=(GetCSVcell(posCSV[i],8));
      cdsPos.Post();
    end;
  end;
  cdsPos.First();
  posCSV.Free();
end;

procedure TFormEditPos.SavePos;
var
  s:string;
  posCSV:TStringList;
begin
  posCSV:=TStringList.Create();
  posCSV.add('Code;Short Description;Long Description;Price1;Price2;Price3;Price4;Price5;Stock');
  try cdsPos.Post(); except end;
  cdsPos.First();
  while not cdsPos.Eof do
  begin
    s:=';'; // Attention pas trop
    s:=SetCSVcell(s,0,(cdsPosCode.Value));
    s:=SetCSVcell(s,1,(cdsPosShort.Value));
    s:=SetCSVcell(s,2,(cdsPosLong.Value));
    s:=SetCSVcell(s,3,(cdsPosPrice1.Value));
    s:=SetCSVcell(s,4,(cdsPosPrice2.Value));
    s:=SetCSVcell(s,5,(cdsPosPrice3.Value));
    s:=SetCSVcell(s,6,(cdsPosPrice4.Value));
    s:=SetCSVcell(s,7,(cdsPosPrice5.Value));
    s:=SetCSVcell(s,8,(cdsPosStock.Value));
    s:=copy(s,1,length(s)-1);
    posCSV.add(s);
    cdsPos.Next();
  end;
  SaveStringsToCSV(ExtractFilePath(Application.Exename)+'pos.csv', posCSV.Text);
  posCSV.Free();
end;

procedure TFormEditPos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SavePos();
end;

end.
