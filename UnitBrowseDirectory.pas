unit UnitBrowseDirectory;

interface

uses Forms, Windows, ShlObj;

function BrowseDialog(const Title: string;
  const Flag: integer ;
  const initialFolder: String): string;

implementation

var
  lg_StartFolder: String;

function BrowseForFolderCallBack(Wnd: HWND; uMsg: UINT; lParam,
lpData: LPARAM): Integer stdcall;
begin
  if uMsg = BFFM_INITIALIZED then
    SendMessage(Wnd,BFFM_SETSELECTION, 1, Integer(@lg_StartFolder[1]));
  result := 0;
end;

function BrowseDialog(const Title: string;
  const Flag: integer ;
  const initialFolder: String): string;
var
  lpItemID : PItemIDList;
  BrowseInfo : TBrowseInfo;
  DisplayName : array[0..MAX_PATH] of char;
  TempPath : array[0..MAX_PATH] of char;
begin
  Result:='';
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
  lg_StartFolder := initialFolder;
  with BrowseInfo do begin
    hwndOwner := Application.Handle;
    if initialFolder <> '' then
      lpfn := BrowseForFolderCallBack;
    pszDisplayName := @DisplayName;
    lpszTitle := PChar(Title);
    ulFlags := Flag;
  end;
  lpItemID := SHBrowseForFolder(BrowseInfo);
  if lpItemId <> nil then begin
    SHGetPathFromIDList(lpItemID, TempPath);
    Result := TempPath;
    GlobalFreePtr(lpItemID);
  end;
end;

function BrowseDialog2(const Title: string;
  const DefaultFolder: string;
  const Flag: integer): string;
begin
end;

end.
