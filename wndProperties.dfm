object FormProperties: TFormProperties
  Left = 711
  Top = 359
  BorderStyle = bsToolWindow
  Caption = 'Properties'
  ClientHeight = 202
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnOK: TBitBtn
    Left = 152
    Top = 168
    Width = 65
    Height = 23
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = btnOKClick
    NumGlyphs = 2
  end
  object btnCancel: TBitBtn
    Left = 224
    Top = 168
    Width = 65
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    NumGlyphs = 2
  end
  object GroupBoxText: TGroupBox
    Left = 8
    Top = 8
    Width = 353
    Height = 97
    Caption = '&Text'
    TabOrder = 0
    object SpeedButtonClearText: TSpeedButton
      Left = 328
      Top = 16
      Width = 17
      Height = 22
      Caption = 'X'
      OnClick = SpeedButtonClearTextClick
    end
    object SpeedButtonItems: TSpeedButton
      Left = 312
      Top = 16
      Width = 17
      Height = 22
      Caption = '...'
      OnClick = SpeedButtonItemsClick
    end
    object EditTitle: TEdit
      Left = 128
      Top = 16
      Width = 185
      Height = 21
      TabOrder = 0
    end
    object GroupBoxFont: TGroupBox
      Left = 8
      Top = 40
      Width = 281
      Height = 49
      Caption = '&Font'
      TabOrder = 1
      object LabelFontName: TLabel
        Left = 10
        Top = 20
        Width = 75
        Height = 13
        Caption = 'LabelFontName'
        OnClick = LabelFontClick
      end
      object LabelFontSize: TLabel
        Left = 104
        Top = 20
        Width = 20
        Height = 13
        Caption = 'Size'
        OnClick = LabelFontClick
      end
      object CheckBoxBold: TCheckBox
        Left = 136
        Top = 9
        Width = 49
        Height = 17
        Caption = '&Bold'
        TabOrder = 0
      end
      object CheckBoxItalic: TCheckBox
        Left = 136
        Top = 25
        Width = 49
        Height = 17
        Caption = '&Italic'
        TabOrder = 1
      end
      object CheckBoxMobile: TCheckBox
        Left = 192
        Top = 9
        Width = 49
        Height = 17
        Caption = '&Mobile'
        TabOrder = 2
      end
    end
    object GroupBoxPen: TGroupBox
      Left = 296
      Top = 40
      Width = 49
      Height = 49
      Caption = 'Color'
      TabOrder = 2
      object PanelPen: TPanel
        Left = 8
        Top = 16
        Width = 33
        Height = 25
        Color = 8454016
        TabOrder = 0
        OnClick = PanelColorClick
      end
    end
    object ComboBoxType: TComboBox
      Left = 8
      Top = 16
      Width = 113
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 3
      Text = 'Free Text'
      Items.Strings = (
        'Free Text'
        'Short Description'
        'Long Description'
        'Price1'
        'Price2'
        'Price3'
        'Price4'
        'Price5')
    end
  end
  object GroupBoxCoordonnees: TGroupBox
    Left = 8
    Top = 112
    Width = 137
    Height = 81
    Caption = '&Position'
    TabOrder = 1
    object Label5: TLabel
      Left = 8
      Top = 19
      Width = 7
      Height = 13
      Caption = '&X'
      FocusControl = EditX
    end
    object Label6: TLabel
      Left = 72
      Top = 19
      Width = 7
      Height = 13
      Caption = '&Y'
      FocusControl = EditY
    end
    object EditX: TEdit
      Left = 24
      Top = 16
      Width = 41
      Height = 21
      MaxLength = 5
      TabOrder = 0
    end
    object EditY: TEdit
      Left = 88
      Top = 16
      Width = 41
      Height = 21
      MaxLength = 5
      TabOrder = 1
    end
    object Button1: TButton
      Left = 8
      Top = 48
      Width = 121
      Height = 25
      Caption = 'Align to grid'
      TabOrder = 2
      OnClick = Button1Click
    end
  end
  object BitBtn1: TBitBtn
    Left = 296
    Top = 168
    Width = 65
    Height = 23
    Caption = 'Delete'
    TabOrder = 4
    OnClick = BitBtn1Click
    NumGlyphs = 2
  end
  object GroupBox1: TGroupBox
    Left = 152
    Top = 112
    Width = 209
    Height = 49
    Caption = 'R'#233'serv'#233' pour fichier fond'
    TabOrder = 5
    object EditReserved: TEdit
      Left = 8
      Top = 16
      Width = 193
      Height = 21
      TabOrder = 0
    end
  end
  object ColorDialog1: TColorDialog
    Left = 264
    Top = 40
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [fdNoSimulations, fdNoStyleSel]
    Left = 80
    Top = 80
  end
end
