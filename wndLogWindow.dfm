object frmLogWindow: TfrmLogWindow
  Left = 793
  Top = 342
  BorderStyle = bsSingle
  ClientHeight = 164
  ClientWidth = 1067
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object lbLog: TListBox
    Left = 0
    Top = 33
    Width = 1067
    Height = 131
    Align = alClient
    ItemHeight = 13
    TabOrder = 0
  end
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 1067
    Height = 33
    Align = alTop
    TabOrder = 1
    object cmbMonitor: TComboBox
      Left = 6
      Top = 6
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object tmrHour: TTimer
    OnTimer = tmrHourTimer
    Left = 96
    Top = 48
  end
end
